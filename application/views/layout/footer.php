        </div>
    </div>
    <div class="clearfix"></div>
    <footer>
        <div class="inner">
            <div class="copy">&copy2014 Kufa, sociedade de advogados - Todos os direitos reservados</div>
            <div class="trupe">criação de sites: <a href="http://trupe.net" target="_blank">Trupe Agência Criativa</a></div>
        </div>
    </footer>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.8.2.min.js"><\/script>')</script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
        <script src="<?php echo base_url(); ?>assets/fancybox/source/jquery.fancybox.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript">
          $(".noticia-bloco:nth-child(3)").css({'margin-right' : '0'});
        </script>
        </body>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            var _gaq=[['_setAccount','UA-43554777-1'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
    </body>
</html>
