<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
        <?php echo $this->seo->get_title($titulo); ?>
        <link rel="shortcut icon" href="favicon.ico">
        <meta name="description" content="Escritório de Advocacia - Sociedade de Advogados especializados em Direito Público">
        <meta name="keywords" content="Escritório de Advocacia - Sociedade de Advogados especializados em Direito Público, Administrativo, Criminal, Eleitoral, Tributário, Terceiro setor, Infraestrutura, Urbanístico, Ambiental, Agrário, Águas e Energia.">
        <meta name="author" content="Trupe Agência Criativa">
        <meta name="viewport" content="width=1024, initial-scale=1">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/normalize.css">
        <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/prettyPhoto.css" type="text/css" media="screen" charset="utf-8" />
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <header>

            <div class="linha-topo"></div>

            <div class="inner">
                <div class="clearfix"></div>

                <a href="<?php echo base_url(); ?>" id="logo-maior">
                    <img src="<?php echo base_url(); ?>assets/img/logo-maior.png" alt="Kufa, sociedade de advogados">
                </a>
                <div id="frase">
                    <p>Especializados em Direito Público</p>
                </div>
                <div class="contato">
                    <?php echo Modules::run('contato/endereco', 'header'); ?>
                </div>
                <div class="clearfix"></div>

                <div class="linha-pre"></div>
                <a href="<?php echo base_url(); ?>" id="logo-menor">
                     <img src="<?php echo base_url(); ?>assets/img/logo-menor.png" alt="Kufa, sociedade de advogados">
                </a>
                <div class="linha-pos"></div>

                <nav>
                    <ul>
                        <li><a <?php echo ($slug == 'home') ? 'class="active"' : ''; ?> id="home" href="<?php echo site_url(); ?>">Home</a></li>
                        <li><a <?php echo ($slug == 'apresentacao') ? 'class="active"' : ''; ?> id="apresentacao" href="<?php echo site_url('apresentacao'); ?>">Apresentação</a></li>
                        <li><a <?php echo ($slug == 'equipe') ? 'class="active"' : ''; ?> id="equipe" href="<?php echo site_url('equipe'); ?>">Equipe</a></li>
                        <li><a <?php echo ($slug == 'atuacao') ? 'class="active"' : ''; ?> id="atuacao" href="<?php echo site_url('atuacao'); ?>">Atuação</a></li>
                        <li><a <?php echo ($slug == 'noticias') ? 'class="active"' : ''; ?> id="noticias" href="<?php echo site_url('noticias'); ?>">Notícias e Publicações</a></li>
                        <li><a <?php echo ($slug == 'agenda') ? 'class="active"' : ''; ?>id="agenda" href="<?php echo site_url('agenda'); ?>">Agenda</a></li>
                        <li><a <?php echo ($slug == 'contato') ? 'class="active"' : ''; ?>id="contato" href="<?php echo site_url('contato'); ?>">Contato</a></li>
                    </ul>
                </nav>
            </div>
        </header>
        <div id="main" class="<?php echo $slug; ?>">
            <div class="inner">
