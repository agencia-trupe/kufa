<div class="span3">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <?php if($this->tank_auth->is_role('admin')) :?>
              <li class="nav-header">Clientes</li>
              <li><a href="#"><i class="icon-plus"></i>Cadastrar cliente</a></li>
              <li><a href="#"><i class="icon-th-list"></i>Exibir clientes</a></li>
              <?php endif; ?>
              <li class="nav-header">Vagas</li>
              <?php if($this->tank_auth->is_role('admin')) :?>
              <li><a href="<?php echo base_url();?>index.php/jobs/setjob"><i class="icon-plus"></i>Cadastrar vaga</a></li>
              <?php endif; ?>
              <li><?php echo anchor('jobs/listjobs', '<i class="icon-th-list"></i>Exibir vagas'); ?></li>
              <li><a href="#"><i class="icon-share-alt"></i>Anunciar nos grupos</a></li>
              <li><a href="#"><i class="icon-comment"></i>Publicar nas redes sociais</a></li>
              <li class="nav-header">Candidatos</li>
              <li><a href="#"><i class="icon-plus"></i>Cadastrar</a></li>
              <li><a href="#"><i class="icon-comment"></i>Convidar</a></li>
              <li><a href="#"><i class="icon-search"></i>Pesquisar</a></li>
              <li><a href="../show"><i class="icon-search"></i>Painel de vagas</a></li>
            </ul>
          </div><!--/.well -->
        </div><!--/span-->