<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Link
*
* Model responsável pelas regras de negócio das galerias de links
*
* @author Nilton de Freitas - Trupe
* @link http://trupe.net
*
*/
Class Link extends Datamapper{
    var $table = 'links';

     public function __construct()
    {
        // model constructor
        parent::__construct();
    }
    /**
     * Retorna os links ativos em ordem crescente de antiguidade
     * @param    int    $limit   quantidade limite por retorno
     * @param    int    $offset  offset para a paginação
     * @return   array           array com a lista de links ativos
     */
    function get_all($limit, $offset)
    {
        $link = new link();
        $link->order_by('titulo', 'asc');
        $link->get($limit, $offset);

        $arr = array();
        foreach($link->all as $links)
        {
            $arr[] = $links;
        }

        return $arr;

    } 

    function get_all_admin()
    {
        $link = new link();
        $link->order_by('titulo', 'asc');
        $link->get();

        $arr = array();
        foreach($link->all as $links)
        {
            $arr[] = $links;
        }

        return $arr;

    } 

    function get_home()
    {
        $link = new link();
        $link->order_by('link_data_expiracao', 'asc');
        $link->get(2);

        $arr = array();
        foreach($link->all as $links)
        {
            $arr[] = $links;
        }

        return $arr;

    }

    function get_all_all($limit, $offset)
    {
        $link = new link();
        $link->order_by('link_data_expiracao', 'asc');
        $link->order_by('link_data_cadastro', 'desc');
        $link->get($limit, $offset);

        $arr = array();
        foreach($link->all as $links)
        {
            $arr[] = $links;
        }

        return $arr;

    }



    function cadastra($dados){

                $link = new link();

                $link->titulo = $dados['titulo'];
                $link->link = $dados['link'];

                if($link->save())
                {
                    $result = TRUE;
                }

            return $result;

    }

    function atualiza_imagem($dados){

        $link = new link();
        $link->where('id', $dados['id']);
        $update = $link->update(array(
            'link_categoria' => $dados['categoria'],
            'link_empresa' => $dados['empresa'],
            'link_link' => $dados['link'],
            'link_imagem' => $dados['imagem'],
            'link_data_expiracao' => strtotime(str_replace('/', '-',
                $dados['data_expiracao'])),
            'link_destaque' => $dados['destaque'],
        ));
        return $update;
    }

    function atualiza($dados){

        $link = new link();
        $link->where('id', $dados['id']);
        $update = $link->update(array(
            'titulo' => $dados['titulo'],
            'link' => $dados['link']
        ));
        return $update;
    }
    function get_link($id)
    {
        $link = new link();
        $link->where('id', $id);
        $link->limit(1);
        $link->get();

        return $link;
    }

     function delete_link($id){
        $link = new link();
        $link->where('id', $id)->get();
        

        if($link->delete()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }
    
}