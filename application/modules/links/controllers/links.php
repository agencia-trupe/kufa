<?php

Class Links extends MX_Controller
{

    public function index()
    {
                $this->lista();
    }

    public function lista()
    {

            //Lógica da paginação de resultados
            $this->load->library('pagination');
            $tipo = $this->uri->segment(2);
            $pagination_config = array(
                    'base_url'       => base_url() . 'links/lista/',
                    'total_rows'     => $this->db->get('links')->num_rows(),
                    'per_page'       => 10,
                    'num_links'      => 5,
                    'next_link'      => 'Próximo <div id="icon-proximo"></div>',
                    'prev_link'      => '<div id="icon-anterior"></div> Anterior',
                    'first_link'     => FALSE,
                    'last_link'      => FALSE, 
                    'full_tag_open'  => '<div class="pagination center"><ul>',
                    'full_tag_close' => '</ul></div>',
                    'cur_tag_open'   => '<li class="active"><a href="#">',
                    'cur_tag_close'  => '</a></li>',
                    'num_tag_open'   => '<li>',
                    'num_tag_close'  => '</li>',
                    'next_tag_open'   => '<li>',
                    'next_tag_close'  => '</li>',
                    'prev_tag_open'   => '<li>',
                    'prev_tag_close'  => '</li>',
            );
            $this->pagination->initialize($pagination_config);
            //Obtendo resultados no banco
            $this->load->model('links/link');
            $data['links'] = $this->link->get_all($pagination_config['per_page'], $this->uri->segment(3));

            //Layout
            $data['titulo'] = 'links';
            $data['slug'] = 'links';
            $data['conteudo_principal'] = "links/lista";
            $this->load->view('layout/template', $data);
    }

}