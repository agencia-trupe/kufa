<h1 class="page">Links</h1>
<img src="<?php echo base_url(); ?>assets/img/links.jpg" alt="">
<div class="sidebar">
    <div class="links">
        <ul>
            <?php foreach($links as $link): ?>
                <li><a href="<?php echo $link->link; ?>"><?php echo $link->titulo . ' - ' . $link->link; ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
    <?php echo $this->pagination->create_links(); ?>
</div>
<div class="clearfix"></div>