<div class="span12">

<legend>
	<?php if (isset($midia)): ?>
		<?php echo $acao == 'editar' ? 'Editar clipping' : 'Novo clipping' ;?>
	<?php else: ?>
		<?php echo $acao == 'editar' ? 'Editar noticia' : 'Nova noticia' ;?>
	<?php endif; ?>
</legend>
	<?php if (isset($error)): ?>
		<div class="alert alert-error">
			<?php echo $error ?>
		</div>
	<?php endif ?>
	<?php
	if(isset($midia))
	{
	  $categoria = array('midia' => 'Na Mídia');
	}
	else
	{
		$categoria = array(
	        'noticias' => 'Notícias',
	        'obras-artigos' => 'Obras | Artigos',
	        'informativos-eleitorais'   =>  'Informativos Eleitorais',
	        'informativos-municipais'   =>  'Informativos Municipais',
	        'informativos-tributarios'  =>  'Informativos Tributários',
	        'manuais-candidatos'        =>  'Manuais do Candidato',
	        'manuais-prefeito'          =>  'Manuais do Prefeito'
	    );
	}
	if(isset($midia))
	{
		echo form_open_multipart(($acao == 'editar') ? 'painel/midia/edit' : 'painel/midia/add', 'class="well"'); 
	}
	else
	{
		echo form_open_multipart(($acao == 'editar') ? 'painel/noticias/edit' : 'painel/noticias/add', 'class="well"'); 
	}
	?>

	<?php if($acao == 'editar') : ?>
	<?php echo
	form_hidden('id', set_value('id', $noticia->id));
	?>
  <?php endif; ?>
	<?php if (isset($midia)): ?>
	  	<?php echo form_hidden('midia', 1) ?>
	<?php endif ?>
  <div class="row-fluid">
	 <div class="span4">
			<label for="categoria">Categoria</label>
			<?php echo form_dropdown('categoria', $categoria, ($acao === 'editar') ? $noticia->categoria : '', 'class="span11"'); ?>
			<?php echo form_error('categoria'); ?>
		</div>
	  </div>
	<div class="row-fluid">
		<div class="span9">
			<label for="">Título</label>
			<?php echo form_input(array('name'=>'titulo', 'id' => 'titulo', 'value'=>set_value('titulo', $acao == 'editar' ? $noticia->titulo : ''), 'class' => 'span12',)); ?>
			<?php echo form_error('titulo'); ?>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span9">
			<label for="">Resumo</label>
			<?php echo form_input(array('name'=>'resumo', 'id' => 'resumo', 'value'=>set_value('resumo', $acao == 'editar' ? $noticia->resumo : ''), 'class' => 'span12',)); ?>
			<?php echo form_error('resumo'); ?>
		</div>
	</div>
	<br>
  <div class="row-fluid">
	  <div class="clearfix"></div>
	  <div class="control-group">
			<label class="control-label" for="conteudo">Conteúdo</label>
			<div class="controls">
			  <?php echo form_textarea('conteudo', set_value('conteudo', $acao == 'editar' ? $noticia->conteudo : ''), 'class="tinymce span11"'); ?>
			  <span class="help-inline"><?php echo form_error('conteudo'); ?></span>
			</div>
	  </div>
	  <div class="control-group">
			<label class="control-label" for="video">Vídeo (código html | largura: 580)</label>
			<div class="controls">
			  <?php echo form_textarea('video', set_value('video', $acao == 'editar' ? $noticia->video : ''), 'class="span11"'); ?>
			  <span class="help-inline"><?php echo form_error('video'); ?></span>
			</div>
	  </div>
	  <div class="control-group">
			<label class="control-label" for="data_publicacao">Data de publicação</label>
			<div class="controls">
			  <?php echo form_input('data_publicacao', set_value('data_publicacao', $acao == 'editar' ? date('d/m/Y', $noticia->data_publicacao) : ''), 'id="datepicker"'); ?>
			  <span class="help-inline"><?php echo form_error('data_publicacao'); ?></span>
			</div>
	  </div>
	 <?php if($acao == 'editar'): ?>
	 <?php if($noticia->noticia_imagem):?>
	 <img src="<?php echo base_url(); ?>assets/img/noticias/<?php echo $noticia->noticia_imagem; ?>" alt="<?php echo $noticia->noticia_titulo; ?>" >
	 <?php endif; ?>
	 <?php endif; ?>
	 <div class="control-group">
			<label class="control-label" for="imagem"><?php echo ($acao == 'editar' ? 'Adicionar / Alterar Imagem' : 'Imagem'); ?></label>
			<div class="controls">
			  <?php echo form_upload('imagem', set_value('imagem'), 'id="datepicker"'); ?>
			  <span class="help-inline"><?php echo form_error('imagem'); ?></span>
			</div>
	 </div>
	 <div class="control-group">
			<label class="control-label" for="arquivo"><?php echo ($acao == 'editar' ? 'Alterar Arquivo' : 'Adicionar Arquivo'); ?></label>
			<div class="controls">
			  <?php echo form_upload('arquivo', set_value('arquivo')); ?>
			  <span class="help-inline"><?php echo form_error('arquivo'); ?></span>
			</div>
	 </div>
  </div>
  <?php echo form_submit('', ($acao == 'editar') ? 'Salvar' : 'Cadastrar', 'class="btn btn-primary"'); ?>
  <?php echo form_close(); ?> 
</div>