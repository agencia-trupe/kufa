<div class="span12">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="span6">
            <legend>
                <?php if (isset($midia)): ?>
                    Na Mídia
                    <?php echo anchor('painel/midia/add', 'Cadastrar', 'class="btn btn-info btn-mini"'); ?>    
                <?php else: ?>
                    Notícias
                    <?php echo anchor('painel/noticias/add', 'Cadastrar', 'class="btn btn-info btn-mini"'); ?>
                <?php endif; ?>
            </legend>
        </div>
    </div>
  <?php if(isset($result)): ?>
     <table class="table table-hovered table-condensed">
        <thead>
            <tr>
                <th>Data de Publicação</th><th>Título</th><th class="span2"><i class="icon-cog"></i></th>
            </tr>
        </thead>
        <?php foreach ($result as $item): ?>
            <tr>
                <td><?php echo date('d/m/Y', $item->data_publicacao); ?></td>
                <td><?php echo $item->titulo ?></td>
                <td>
                    <?php if (isset($midia)): ?>
                        <?php echo anchor('painel/midia/edit/' . $item->id, 'Editar', 'class="btn btn-info btn-mini btn-painel"'); ?>
                    <?php else: ?>
                        <?php echo anchor('painel/noticias/edit/' . $item->id, 'Editar', 'class="btn btn-info btn-mini btn-painel"'); ?>
                    <?php endif; ?>
                    <?php echo form_open('painel/noticias/delete', 'style="display:inline"') ?>
                        <?php echo form_hidden('id', $item->id) ?>
                        <?php if (isset($midia)): ?>
                            <?php echo form_hidden('midia', 1) ?>
                        <?php endif ?>
                        <?php echo form_submit('', 'apagar', 'class="btn btn-mini btn-danger"') ?>
                    <?php echo form_close() ?>
                </td>
            </tr>
        <?php endforeach ?>
    </table>
  <?php endif; ?>
        

</div><!--/span-->