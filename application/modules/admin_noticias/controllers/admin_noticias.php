<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_noticias extends MX_Controller
{
    /**
     * Array de variáveis a ser passado para a view.
     * 
     * @var [type]
     */
    public $data;

    /**
     * Construtor
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('noticias/noticia');
        $this->data['module'] = 'noticias';
    }

    function index(){

        $this->lista();
    }
    /**
    *Lista todos os noticias cadastrados atualmente
    *
    * @return [type] [description]
    */
    function lista($midia = NULL)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Kufa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
          {
            $this->load->library('pagination');
            $pagination_config = array(
                        'per_page'       => 30,
                        'num_links'      => 5,
                        'next_link'      => 'próximo',
                        'prev_link'      => 'anterior',
                        'first_link'     => FALSE,
                        'last_link'      => FALSE, 
                        'full_tag_open'  => '<div class="pagination center"><ul>',
                        'full_tag_close' => '</ul></div>',
                        'cur_tag_open'   => '<li class="active"><a href="#">',
                        'cur_tag_close'  => '</a></li>',
                        'num_tag_open'   => '<li>',
                        'num_tag_close'  => '</li>',
                        'next_tag_open'   => '<li>',
                        'next_tag_close'  => '</li>',
                        'prev_tag_open'   => '<li>',
                        'prev_tag_close'  => '</li>',
                );
                if($midia)
                {
                    $pagination_config['total_rows'] = $this->db
                                                ->where('categoria', 'midia')
                                                ->get('noticias')
                                                ->num_rows();
                    $pagination_config['base_url'] = site_url() . 'painel/midia/lista/';
                }
                else
                {
                    $pagination_config['total_rows'] = $this->db
                                                ->get('noticias')
                                                ->num_rows();
                    $pagination_config['base_url'] = site_url() . 'painel/noticias/lista/';
                }

                $this->pagination->initialize($pagination_config);
                //Obtendo resultados no banco
                $this->load->model('noticias/noticia');

                if($midia)
                {
                    $data['result'] = $this->noticia
                            ->get_all_all(  $pagination_config['per_page'], 
                                            $this->uri->segment(3),
                                            TRUE
                                            );

                    $data['midia'] = TRUE;
                    $data['module'] = 'midia';
                }
                else
                {
                    $data['result'] = $this->noticia
                            ->get_all_all(  $pagination_config['per_page'], 
                                            $this->uri->segment(3)
                                            );
                }

                $data['title'] = 'Kufa';

                if($midia)
                {
                    $data['module'] = 'midia';
                }
                else
                {
                    $data['module'] = 'noticias';
                }

                $data['main_content'] = 'n_lista_view';
                $this->load->view('includes/template', $data);
          }
          else
          {
          Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de 
            acesso area administrativa sem privilégios');
          $this->session->set_flashdata('error', 'Erro de permissão. 
                                                Você precisa ser 
                                                administrador para realizar essa ação');
          redirect();

          } 
        }
    }

    /**
     * Edita uma notícia.
     *
     * @param [int] $noticia_id [description]
     * 
     * @return [mixed]     [description]
     */
    function edit($noticia_id = NULL, $midia = NULL)
    {
        if($midia)
            $this->data['midia'] = TRUE;
            $this->data['module'] = 'midia';

        $post = array();
        //Define o array com os dados do $_POST;
        foreach ($_POST as $chave => $valor) 
        {
            $post[$chave] = $valor;
        }

        //Define a ação como cadastro para passar ao formulário
        $this->data['acao'] = 'editar';

        //Caso não haja post, retorna o forumlário de cadastro
        if( ! $post )
        {
            try 
            {
                $this->data['noticia'] = $this->noticia->get_noticia($noticia_id);
            } 
            catch (Exception $e) 
            {
                //Retorna a página de listagem caso nenhuma página corresponda ao id fornecido
                $this->session->set_flashdata('error', $e->getMessage());
                redirect('painel/noticias');
            }
            $this->data['acao'] = 'editar';
            $this->data['main_content'] = 'n_cadastra_view';
            $this->load->view('includes/template', $this->data);
        }
        else
        {
            $noticia = $this->noticia->get_noticia($post['id']);

            //Define a imagem atual, que será apagada em caso de novo upload de imagem
            $old_image = $noticia->imagem;
            $old_file = $noticia->arquivo;

            //Retorna o formulário de cadastro com os erros caso não passe na
            //validação do formulário
            if( ! $this->form_validation->run('noticias') )
                return $this->_form_error_return($this->data['acao'], NULL, $post['id'], 'id');

            //Checa se houve envio de imagem
            if(strlen($_FILES['imagem']['name'])>0)
            {
                //Tenta persistir a imagem e retorna uma exception caso não persista
                try 
                {
                    $upload_image = $this->_upload('imagem');
                    $post['imagem'] = $upload_image['file_name'];
                } 
                catch (Exception $e) 
                {
                    return $this->_form_error_return($this->data['acao'], $e->getMessage(), $post['id']);
                }
            }

            if(strlen($_FILES['arquivo']['name'])>0)
            {
                //Tenta persistir a imagem e retorna uma exception caso não persista
                try 
                {
                    $upload_file = $this->_upload('arquivo');
                    $post['arquivo'] = $upload_file['file_name'];
                } 
                catch (Exception $e) 
                {
                    return $this->_form_error_return($this->data['acao'], $e->getMessage(), $post['id']);
                }
            }

            //Tenta inserir a notícia no banco de dados e retorna uma exception caso haja erro;
            try 
            {
                if(isset($post['midia']))
                    unset($post['midia']);

                $cadastro = explode('/', $post['data_publicacao']);
                $post['data_publicacao'] = strtotime($cadastro[1] . '/' . $cadastro[0] . '/' . $cadastro[2]);
                
                $post['updated'] = $_SERVER['REQUEST_TIME'];
                
                $post['user_id'] = $this->tank_auth->get_user_id();
                
                $this->noticia->atualiza($post);
                
                if($upload_image)
                    unlink($upload_image['file_path'] . $old_image);
                
                if($upload_file)
                    unlink($upload_file['file_path'] . $old_file);
                
                if($post['categoria'] === 'midia')
                {
                    redirect('painel/midia');
                }
                else
                {
                    redirect('painel/noticias');
                }
                
            } 
            catch (Exception $e) 
            {
                if($upload_image)
                    unlink($upload_image['full_path']);
                if($upload_file)
                    unlink($upload_file['full_path']);
                return $this->_form_error_return($this->data['acao'], $e->getMessage(), $post['id'], 'id');
            }
        }
    }

    /**
     * Adiciona uma nova notícia
     *
     * @return void formulário de cadastro
     */
    function add($midia = NULL)
    {
        if($midia)
            $this->data['midia'] = TRUE;
            $this->data['module'] = 'midia';
        $post = array();
        //Define o array com os dados do $_POST;
        foreach ($_POST as $chave => $valor) 
        {
            $post[$chave] = $valor;
        }
        //Define a ação como cadastro para passar ao formulário
        $this->data['acao'] = 'cadastra';

        //Caso não haja post, retorna o forumlário de cadastro
        if( ! count( $post ) )
        {
            if($midia)
                $this->data['module'] = 'midia';
            else
                $this->data['module'] = 'noticias';
            $this->data['main_content'] = 'n_cadastra_view';
            $this->load->view('includes/template', $this->data);
        }
        else
        {
            //Retorna o formulário de cadastro com os erros caso não passe na
            //validação do formulário

            if( ! $this->form_validation->run('noticias') )
            {
                if($post['midia'])
                {
                    return $this->_form_error_return($this->data['acao'], null, null, 'midia');
                }
                else
                {
                    return $this->_form_error_return($this->data['acao']);
                }
            }

            //Checa se houve envio de imagem
            if(strlen($_FILES['imagem']['name'])>0)
            {
                //Tenta persistir a imagem e retorna uma exception caso não persista
                try 
                {
                    $upload_image = $this->_upload('imagem');
                    $post['imagem'] = $upload_image['file_name'];
                } 
                catch (Exception $e) 
                {
                    return $this->_form_error_return($this->data['acao'], $e->getMessage());
                }
            }

            if(strlen($_FILES['arquivo']['name'])>0)
            {
                //Tenta persistir a imagem e retorna uma exception caso não persista
                try 
                {
                    $upload_file = $this->_upload('arquivo');
                    $post['arquivo'] = $upload_file['file_name'];
                } 
                catch (Exception $e) 
                {
                    return $this->_form_error_return($this->data['acao'], $e->getMessage());
                }
            }

            //Tenta inserir a notícia no banco de dados e retorna uma exception caso haja erro;
            try 
            {
                if(isset($post['midia']))
                    unset($post['midia']);
                $cadastro = explode('/', $post['data_publicacao']);
                $post['data_publicacao'] = strtotime($cadastro[1] . '/' . $cadastro[0] . '/' . $cadastro[2]);
                
                $post['created'] = $_SERVER['REQUEST_TIME'];
                $post['user_id'] = $this->tank_auth->get_user_id();
                $this->noticia->cadastra($post);
                 $this->session->set_flashdata('success', 'Notícia cadastrada
                    com sucesso');
                if($post['categoria'] === 'midia')
                {
                    redirect('painel/midia');
                }
                else
                {
                    redirect('painel/noticias');
                }
            } 
            catch (Exception $e) 
            {
                if($upload_image)
                    unlink($upload_image['full_path']);
                if($upload_file)
                    unlink($upload_file['full_path']);
                return $this->_form_error_return($this->data['acao'], $e->getMessage());
            }
        }
    }

    /**
     * Deleta uma notícia
     *  
     * @return void redirect
     */
    function delete()
    {
        $post = $this->input->post();

        try 
        {
            $noticia = $this->noticia->get_noticia($post['id']);
            $imagem = $noticia->imagem;
            $arquivo = $noticia->arquivo;
            $this->noticia->apaga($post['id']);
        } 
        catch (Exception $e) 
        {
            $this->session->set_flashdata( 'error', $e->getMessage() );
            redirect('painel/noticias');
        }
        if( ! is_null($imagem) )
            unlink('./assets/img/noticias/' . $imagem);
        if( ! is_null($arquivo) )
            unlink('./assets/arquivos/' . $arquivo);

        $this->session->set_flashdata( 'success', 'Registro removido' );
        redirect('painel/noticias');

    }

    /**
     * Retorna o formulário contendo a mensagem de erro
     * 
     * @param string $acao       [cadastro|edição]
     * @param string $error      mensagem de erro
     * @param mixed  $value      valor a buscar no banco de dados
     * @param string $identifier identificador do valor passado
     * 
     * @return void formulário
     */
    private function _form_error_return($acao, $error = NULL, $noticia_id = NULL, $local = NULL)
    {
        if( $acao === 'editar' )
            $this->data['noticia'] = $this->noticia->get_noticia($noticia_id);
        
        if( $error )
            $this->data['error'] = $error;

        if($local)
            $this->data['midia'] = TRUE;

        $this->data['acao'] = $acao;
        $this->data['main_content'] = 'n_cadastra_view';
        $this->load->view('includes/template', $this->data);
    }

    /**
     * Realiza o upload de um arquivo
     *
     * @throws Exception If cant do upload
     * @return void dados de upload
     */
    private function _upload($campo)
    {
        if($campo === 'imagem')
        {
            //Configurações para upload de imagem
            $config['upload_path'] = './assets/img/noticias/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '8000';
            $config['max_width']  = '3000';
            $config['max_height']  = '3000';

            $this->load->library('upload', $config);

            //Retorna erro em caso de upload não realizado
            if ( ! $this->upload->do_upload($campo) )
                throw new Exception($campo . ' ' . $this->upload->display_errors(), 1);

            $this->load->library('image_moo');

            $upload_data = $this->upload->data();
            $file_uploaded = $upload_data['full_path'];
            $thumb = $upload_data['file_path'] . './thumbs/' . $upload_data['file_name'];

            //Locais dos arquivos alterados
            $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];

            //Redimensiona a imagem
            if ( ! $this->image_moo->load($file_uploaded)
                                    ->resize(600,600,FALSE)
                                    ->save($new_file,true) )
                throw new Exception("Erro ao redimensionar imagem", 1);

            //Cria uma miniatura da imagem
            if ( ! $this->image_moo->load($file_uploaded)
                                     ->resize_crop(200,120)
                                     ->save($thumb, true) )
                throw new Exception('Erro ao criar miniatura', 1);
        }
        else
        {
            //Configurações para upload de arquivos
            $config['upload_path'] = './assets/arquivos/';
            $config['allowed_types'] = 'txt|rtf|odt|doc|docx|xls|xlsx|ppt|pps|pdf';
            $config['max_size'] = '16000';

            if($this->data['acao'] == 'editar')
                $this->load->library('upload');
            $this->upload->initialize($config);

            //Retorna erro em caso de upload não realizado
            if ( ! $this->upload->do_upload($campo) )
                throw new Exception($campo . ' ' . $this->upload->display_errors(), 1);
        }
       
       
        return $this->upload->data();

    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */