<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Foto
*
* Model responsável pelas regras de negócio das galerias de fotos
*
* @author Nilton de Freitas - Trupe
* @link http://trupe.net
*
*/
Class Foto extends Datamapper{
    var $table = 'fotos';

     public function __construct()
    {
        // model constructor
        parent::__construct();
    }
    /**
     * Retorna os fotos ativos em ordem crescente de antiguidade
     * @param    int    $limit   quantidade limite por retorno
     * @param    int    $offset  offset para a paginação
     * @return   array           array com a lista de fotos ativos
     */
    function get_all($limit, $offset)
    {
        $foto = new Foto();
        $foto->order_by('id', 'desc');
        $foto->get($limit, $offset);

        $arr = array();
        foreach($foto->all as $fotos)
        {
            $arr[] = $fotos;
        }

        return $arr;

    } 

    function get_all_admin($limit, $offset)
    {
        $foto = new Foto();
        $foto->order_by('id', 'asc');
        $foto->get($limit, $offset);

        $arr = array();
        foreach($foto->all as $fotos)
        {
            $arr[] = $fotos;
        }

        return $arr;

    } 

    function get_home()
    {
        $foto = new Foto();
        $foto->order_by('foto_data_expiracao', 'asc');
        $foto->get(2);

        $arr = array();
        foreach($foto->all as $fotos)
        {
            $arr[] = $fotos;
        }

        return $arr;

    }

    function get_all_all($limit, $offset)
    {
        $foto = new Foto();
        $foto->order_by('foto_data_expiracao', 'asc');
        $foto->order_by('foto_data_cadastro', 'desc');
        $foto->get($limit, $offset);

        $arr = array();
        foreach($foto->all as $fotos)
        {
            $arr[] = $fotos;
        }

        return $arr;

    }



    function salva_fotos($dados, $titulos, $descricao){

            $tamanho = count($dados)-1;

            for($f=0; $f<=$tamanho; $f++){

                $foto = new Foto();

                $foto->imagem = $dados[$f]['file_name'];
                $foto->titulo = $titulos[$f];
                $foto->descricao = $descricao[$f];


                if($foto->save())
                {
                    $result = TRUE;
                }

            }
            return $result;

    }

    function atualiza_imagem($dados){

        $foto = new Foto();
        $foto->where('id', $dados['id']);
        $update = $foto->update(array(
            'foto_categoria' => $dados['categoria'],
            'foto_empresa' => $dados['empresa'],
            'foto_link' => $dados['link'],
            'foto_imagem' => $dados['imagem'],
            'foto_data_expiracao' => strtotime(str_replace('/', '-',
                $dados['data_expiracao'])),
            'foto_destaque' => $dados['destaque'],
        ));
        return $update;
    }

    function atualiza($id, $titulo, $descricao){

        $foto = new Foto();
        $foto->where('id', $id);
        $update = $foto->update(array(
            'titulo' => $titulo,
            'descricao' => $descricao
        ));
        return $update;
    }
    function get_foto($id)
    {
        $foto = new Foto();
        $foto->where('id', $id);
        $foto->limit(1);
        $foto->get();

        return $foto;
    }

     function delete_foto($id){
        $foto = new Foto();
        $foto->where('id', $id)->get();
        

        if($foto->delete()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }
    
}