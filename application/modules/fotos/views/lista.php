<h1 class="page">Fotos</h1>
    <div class="clearfix"></div>
    <div class="thumbs">
    	<?php foreach($fotos as $foto): ?>
            <a data-caption="Teste de legenda" class="galeria" rel="prettyPhoto['fotos']" href="<?php echo base_url(); ?>assets/img/fotos/display/<?php echo $foto->imagem; ?>" 
                 title="<?php echo $foto->descricao; ?>">
                <img id="thumb" src="<?php echo base_url(); ?>assets/img/fotos/thumbs/<?php echo $foto->imagem; ?>" alt="<?php echo $foto->titulo; ?>">
                <h1><?php echo $foto->titulo; ?></h1>
            </a>
        <?php endforeach; ?>
        <div class="clearfix"></div>
        <?php echo $this->pagination->create_links(); ?>
    </div>
<div class="clearfix"></div>