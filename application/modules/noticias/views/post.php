<h1 class="page">Notícias e Publicações</h1>
<div class="sidebar">
    <div class="links">
        <ul>
<?php $a = $this->noticia->get_all('noticias', 1, 0); if (!empty($a)): ?>
            <li>
                <a class="<?php echo ($post->categoria == 'noticias') ?
                    'active' : ''; ?>" href="<?php echo site_url('noticias/'); ?>">
                    Notícias
                </a>
            </li>
<?php endif; ?>
<?php $b = $this->noticia->get_all('obras-artigos', 1, 0); if (!empty($b)): ?>
            <li>
                <a class="<?php echo ($post->categoria == 'obras-artigos') ?
                    'active' : ''; ?>" href="<?php echo site_url('noticias/obras-artigos/'); ?>">
                    Obras e Artigos
                </a>
            </li>
<?php endif; ?>
<?php $c = $this->noticia->get_all('informativos-eleitorais', 1, 0); if (!empty($c)): ?>
            <li>
                <a class="<?php echo ($post->categoria == 'informativos-eleitorais') ?
                    'active' : ''; ?>" href="<?php echo site_url('noticias/informativos-eleitorais'); ?>">
                    Informativos Eleitorais
                </a>
            </li>
<?php endif; ?>
<?php $d = $this->noticia->get_all('informativos-municipais', 1, 0); if (!empty($d)): ?>
            <li>
                <a class="<?php echo ($post->categoria == 'informativos-municipais') ?
                    'active' : ''; ?>" href="<?php echo site_url('noticias/informativos-municipais'); ?>">
                    Informativos Municipais
                </a>
            </li>
<?php endif; ?>
<?php $e = $this->noticia->get_all('informativos-tributarios', 1, 0); if (!empty($e)): ?>
            <li>
                <a class="<?php echo ($post->categoria == 'informativos-tributarios') ?
                    'active' : ''; ?>" href="<?php echo site_url('noticias/informativos-tributarios'); ?>">
                    Informativos Tributários
                </a>
            </li>
<?php endif; ?>
<?php $f = $this->noticia->get_all('manuais-candidato', 1, 0); if (!empty($f)): ?>
            <li>
                <a class="<?php echo ($post->categoria == 'manuais-candidato') ?
                    'active' : ''; ?>" href="<?php echo site_url('noticias/manuais-candidato'); ?>">
                    Manuais do Candidato
                </a>
            </li>
<?php endif; ?>
<?php $g = $this->noticia->get_all('manuais-prefeito', 1, 0); if (!empty($g)): ?>
            <li>
                <a class="<?php echo ($post->categoria == 'manuais-prefeito') ?
                    'active' : ''; ?>" href="<?php echo site_url('noticias/manuais-prefeito'); ?>">
                    Manuais do Prefeito
                </a>
            </li>
<?php endif; ?>
        </ul>
    </div>
</div>
<div class="content">
    <div class="text post">
        <article>
        <div class="conteudo">
            <div class="data">
                <p>
                    <?php echo date('d', $post->data_publicacao) . ' de ' 
                    .$this->calendar->get_month_name(date('m', $post->data_publicacao))
                    .' de ' . date('Y', $post->data_publicacao); ?>
                </p>
            </div>
            <h1><?php echo $post->titulo; ?></h1>
            <p class="resumo">
                <?php echo $post->resumo; ?>
            </p>
            <?php if($post->imagem != NULL): ?>
                <a href="<?php echo base_url(); ?>assets/img/noticias/<?php echo $post->imagem; ?>" class="noticia-img">
                    <img src="<?php echo base_url(); ?>assets/img/noticias/thumbs/<?php echo $post->imagem; ?>" alt="<?php echo $post->titulo; ?>">
                </a>
            <?php endif; ?>

            <?php if($post->video != NULL): ?>
                <?php echo $post->video ?>
            <?php endif; ?>

            <?php echo $post->conteudo; ?>
        </div>
        <div class="clearfix"></div>
        <a href="<?php echo site_url('noticias/' . $post->categoria); ?>" class="voltar">Voltar</a>
        </article>
    </div>
</div>
<div class="clearfix"></div>