<div class="noticias-widget">
    <div class="clearfix"></div>
    <h1>Últimas notícias</h1>
    <div class="barra-topo"></div>
    <a href="<?php echo site_url('noticias') ?>" class="todas">veja todas as notícias</a>
    <div class="clearfix"></div>
    <?php if(isset($noticias)): ?>
        <?php foreach($noticias as $noticia): ?>
            <div class="noticia-bloco">
                <header>
                    <h2><a href="<?php echo site_url('noticias/post/' . $noticia->id); ?>"><?php echo $noticia->titulo; ?></a></h1>
                </header>
                <div class="conteudo">
                    <p><a href="<?php echo site_url('noticias/post/' . $noticia->id); ?>"><?php echo substr(strip_tags($noticia->resumo), 0, 86); ?></a>
                    <a class="more" href="<?php echo base_url() . 'noticias/detalhe/' . $noticia->id; ?>">&raquo;</a>
                    </p>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
    <div class="clearfix"></div>
</div>