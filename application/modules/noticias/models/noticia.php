<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* noticia
*
* Model responsável pelas regras de negócio dos noticias de estágio, 
* trainee e aprendiz
*
* @author Nilton de Freitas - Trupe
* @link http://trupe.net
*
*/
Class Noticia extends Datamapper{
    var $table = 'noticias';

     public function __construct()
    {
        // model constructor
        parent::__construct();
    }
    /**
     * Retorna os noticias ativos em ordem crescente de antiguidade
     * @param    int    $limit   quantidade limite por retorno
     * @param    int    $offset  offset para a paginação
     * @return   array           array com a lista de noticias ativos
     */
    function get_all($categoria, $limit, $offset)
    {
        $noticia = new noticia();
        $noticia->where('categoria', $categoria);
        $noticia->order_by('data_publicacao', 'desc');
        $noticia->get($limit, $offset);

        $arr = array();
        foreach($noticia->all as $noticias)
        {
            $arr[] = $noticias;
        }

        return $arr;

    }

    function get_home()
    {
        $noticia = new noticia();
        $noticia->where(array(
                            'categoria !=' => 'reportagens-concedidas'
                        ));
        $noticia->order_by('data_publicacao', 'desc');
        $noticia->get(3);

        $arr = array();
        foreach($noticia->all as $noticias)
        {
            $arr[] = $noticias;
        }

        return $arr;

    }

    function get_entrevista()
    {
        $noticia = new noticia();
        $noticia->where(array(
                    'data_publicacao <=' => time(),
                    'video !=' => '',
                    'categoria =' => 'midia'
                    ));
        $noticia->order_by('data_publicacao', 'desc');
        $noticia->get(1);

        return $noticia;

    }

    function get_all_all($limit, $offset, $midia = NULL)
    {
        $noticia = new noticia();
        if($midia)
            $noticia->where('categoria', 'midia');
        $noticia->order_by('data_publicacao', 'desc');
        $noticia->get($limit, $offset);

        $arr = array();
        foreach($noticia->all as $noticias)
        {
            $arr[] = $noticias;
        }

        return $arr;

    }


    /**
     * Cadastra uma nova notícia
     * 
     * @param array $dados [description]
     * @return boolean;
     */
    function cadastra($dados){

        $noticia = new noticia();

        foreach ($dados as $chave => $valor) {
            $noticia->$chave = $valor;
        }

        if( ! $noticia->save() )
            throw new Exception('Erro ao cadastrar notícia', 1);
            
        return TRUE;
    }

    function atualiza($dados){

        $noticia = new noticia();
        $noticia->where('id', $dados['id']);
        $update_data = array();
        foreach ($dados as $chave => $valor)
        {
            $update_data[$chave] = $valor;
        }
        $update = $noticia->update($update_data);

        if( ! $update )
            throw new Exception('Erro ao atualizar registro', 1);
        return $update;
    }
    function get_noticia($id)
    {
        $noticia = new noticia();
        $noticia->where('id', $id);
        $noticia->limit(1);
        $noticia->get();

        if( ! $noticia->exists() )
            throw new Exception('Nenhuma notícia corresponde ao id', 1);

        return $noticia;
    }

    function get_relate($id)
    {
        $given = new noticia();
        $atual = $given->where('id', $id)->get();

        $noticia = new noticia();
        $noticia->order_by('data_publicacao', 'desc');
        $noticia->where('data_publicacao <', $atual->data_publicacao);
        $noticia->limit(5);
        $noticia->get();

        $arr = array();
        foreach($noticia->all as $noticias)
        {
            $arr[] = $noticias;
        }

        return $arr;
    }

function get_anterior($id)
    {
        $given = new Noticia();
        $atual = $given->where('id', $id)->get();

        $anterior = new Noticia();
        $anterior->order_by('data_publicacao', 'desc');
        $anterior->where('data_publicacao <', $atual->data_publicacao);
        $anterior->limit(1);
        $anterior->get();
        if(!$anterior->exists())
        {
            return FALSE; 
        }
        else
        {
            return $anterior;
        }
    }

    /**
     * Retorna o registro anterior ao registro cujo id foi passado como parâmetro
     * @param  int $id 
     * @return mixed
     */
    function get_proximo($id)
    {
        $given = new Noticia();
        $atual = $given->where('id', $id)->get();

        $proximo = new Noticia();
        $proximo->order_by('data_publicacao', 'asc');
        $proximo->where('data_publicacao >', $atual->data_publicacao);
        $proximo->limit(1);
        $proximo->get();
        if(!$proximo->exists())
        {
            return FALSE; 
        }
        else
        {
            return $proximo;
        }
    }


     function delete_noticia($id){
        $noticia = new noticia();
        $noticia->where('id', $id)->get();


        if($noticia->delete()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }

    /**
     * Remove um registro do banco de dados
     * 
     * @param int $id id do serviço.
     * 
     * @return boolean status
     */
    function apaga($id)
    {
        $noticia = new Noticia();
        $noticia->where('id', $id)->get();
        if($noticia->delete())
        {
            return TRUE;
        }
        return FALSE;
    }

}