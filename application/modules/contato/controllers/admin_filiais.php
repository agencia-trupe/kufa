<?php
/**
* File: admin_filiais.php
* 
* PHP version 5.3
*
* @category Kufa
* @package  Kufa
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Admin_filiais
 * 
 * @category Kufa
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/

class Admin_filiais extends AuthController
{
	/**
	 * Array de variáveis a ser passado para a view.
	 * 
	 * @var array
	 */
	var $data;

	/**
	 * Construtor
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->model('contato/filial');
		$this->data['module'] = 'filiais';
	}

	/**
	 * Ação principal
	 * 
	 * @return [type] [description]
	 */
	function index()
	{

		$this->lista();
	}

	/**
	*Lista todos os filiais cadastrados atualmente
	*
	* @return [type] [description]
	*/
	function lista()
	{
		$this->load->library('pagination');
		$this->load->library('table');
		$pagination_config = array(
					'base_url'       => site_url() . 'painel/filiais/lista/',
					'total_rows'     => $this->db->get('filiais')->num_rows(),
					'per_page'       => 30,
					'num_links'      => 5,
					'next_link'      => 'próximo',
					'prev_link'      => 'anterior',
					'first_link'     => FALSE,
					'last_link'      => FALSE, 
					'full_tag_open'  => '<div class="pagination center"><ul>',
					'full_tag_close' => '</ul></div>',
					'cur_tag_open'   => '<li class="active"><a href="#">',
					'cur_tag_close'  => '</a></li>',
					'num_tag_open'   => '<li>',
					'num_tag_close'  => '</li>',
					'next_tag_open'   => '<li>',
					'next_tag_close'  => '</li>',
					'prev_tag_open'   => '<li>',
					'prev_tag_close'  => '</li>',
			);
		$this->pagination->initialize($pagination_config);

		//Obtendo resultados no banco
		$this->data['filiais'] = $this->filial->
		get_all($pagination_config['per_page'], $this->uri->
			segment(3));

		$this->data['title'] = 'Noticias';
		$this->data['module'] = 'filiais';
		$this->data['conteudo'] = 'contato/admin_filiais_lista';
		$this->load->view('start/template', $this->data);      
	}
	
	/**
	 * Adiciona um novo filial
	 *
	 * @return void formulário de cadastro
	 */
	function add()
	{
		$post = array();
		//Define o array com os dados do $_POST;
		foreach ($_POST as $chave => $valor) 
		{
			$post[$chave] = $valor;
		}
		//Define a ação como cadastro para passar ao formulário
		$this->data['acao'] = 'cadastra';

		//Caso não haja post, retorna o forumlário de cadastro
		if( ! count( $post ) )
		{
			$this->data['module'] = 'filiais';
			$this->data['conteudo'] = 'contato/admin_filiais_edita';
			$this->load->view('start/template', $this->data);
		}
		else
		{
			//Retorna o formulário de cadastro com os erros caso não passe na
			//validação do formulário
			if( ! $this->form_validation->run('filiais') )
				return $this->_form_error_return($this->data['acao']);

			//Tenta inserir a notícia no banco de dados e retorna uma exception caso haja erro;
			try 
			{
				$post['local'] = $post['locale'];
				unset($post['locale']);
				$post['created'] = $_SERVER['REQUEST_TIME'];
				$post['user_id'] = $this->tank_auth->get_user_id();
				$this->filial->insert($post);
				redirect('painel/filiais');
			} 
			catch (Exception $e) 
			{
				if($upload)
					unlink($upload['full_path']);
				return $this->_form_error_return($this->data['acao'], $e->getMessage());
			}
		}
	}

	/**
	 * Edita uma notícia.
	 *
	 * @param [int] $parceiro_id [description]
	 * 
	 * @return [mixed]     [description]
	 */
	function edit($parceiro_id = NULL)
	{
		$post = array();
		//Define o array com os dados do $_POST;
		foreach ($_POST as $chave => $valor) 
		{
			$post[$chave] = $valor;
		}

		//Define a ação como cadastro para passar ao formulário
		$this->data['acao'] = 'editar';

		//Caso não haja post, retorna o forumlário de cadastro
		if( ! $post )
		{
			try 
			{
				$this->data['filial'] = $this->filial->get_conteudo($parceiro_id, 'id');
			} 
			catch (Exception $e) 
			{
				//Retorna a página de listagem caso nenhuma página corresponda ao id fornecido
				$this->session->set_flashdata('error', $e->getMessage());
				redirect('painel/filiais');
			}
			$this->data['acao'] = 'editar';
			$this->data['conteudo'] = 'contato/admin_filiais_edita';
			$this->load->view('start/template', $this->data);
		}
		else
		{
			$filial = $this->filial->get_conteudo($post['id'], 'id');

			//Define a imagem atual, que será apagada em caso de novo upload de imagem
			$old_image = $filial->imagem;

			//Retorna o formulário de cadastro com os erros caso não passe na
			//validação do formulário
			if( ! $this->form_validation->run('filiais') )
				return $this->_form_error_return($this->data['acao'], NULL, $post['id'], 'id');

			//Tenta inserir a notícia no banco de dados e retorna uma exception caso haja erro;
			try 
			{
				$post['local'] = $post['locale'];
				unset($post['locale']);
				$post['updated'] = $_SERVER['REQUEST_TIME'];
				$post['user_id'] = $this->tank_auth->get_user_id();
				$this->filial->change($post);
				redirect('painel/filiais');
			} 
			catch (Exception $e) 
			{
				return $this->_form_error_return($this->data['acao'], $e->getMessage(), $post['id'], 'id');
			}
		}
	}

	/**
	 * Deleta uma notícia
	 *  
	 * @return void redirect
	 */
	function delete()
	{
		$post = $this->input->post();

		try 
		{
			$filial = $this->filial->get_conteudo($post['id'], 'id');
			$imagem = $filial->imagem;
			$this->filial->apaga($post['id']);
		} 
		catch (Exception $e) 
		{
			$this->session->set_flashdata( 'error', $e->getMessage() );
			redirect('painel/filiais');
		}
		unlink('./assets/img/filiais/' . $imagem);
		$this->session->set_flashdata( 'success', 'Registro removido' );
		redirect('painel/filiais');

	}

	/**
	 * Retorna o formulário contendo a mensagem de erro
	 * 
	 * @param string $acao  	 [cadastro|edição]
	 * @param string $error 	 mensagem de erro
	 * @param mixed  $value 	 valor a buscar no banco de dados
	 * @param string $identifier identificador do valor passado
	 * 
	 * @return void formulário
	 */
	private function _form_error_return($acao, $error = NULL, $value = NULL, $identifier = NULL)
	{
		if( $acao === 'editar' )
			$this->data['filial'] = $this->filial->get_conteudo($value, $identifier);
		
		if( $error )
			$this->data['error'] = $error;

		$this->data['module'] = 'filiais';
		$this->data['acao'] = $acao;
		$this->data['conteudo'] = 'contato/admin_filiais_edita';
		$this->load->view('start/template', $this->data);
	}
}

/* End of file admin_filiais.php */
/* Location: ./modules/filiais/controllers/admin_filiais.php */