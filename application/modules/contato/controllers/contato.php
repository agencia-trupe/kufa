<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Contato
* 
* @author Nilton de Freitas - Trupe 
* @link http://trupe.net
*
*/
Class Contato extends MX_Controller
{
    /**
     * Exibe o formulário de contato
     */
    public function index()
    {
            $data['titulo'] = 'Contato';
            $data['slug'] = 'contato';
            $this->load->model('contato/endereco');
            $data['contato'] = $this->endereco->get_endereco();
            $data['conteudo_principal'] = "contato/index";
            $this->load->view('layout/template', $data);
    }

    function endereco($tipo = NULL)
    {
        $this->load->model('endereco');
        $data['endereco'] = $this->endereco->get_endereco();
        if($tipo == 'header')
        {
            $this->load->view('contato/endereco_header', $data);
        }
        elseif($tipo == 'contato')
        {
            $this->load->view('contato/endereco_contato', $data);
        }
        elseif($tipo == 'mail')
        {
            $this->load->view('contato/endereco_mail', $data);
        }
    }

    function filiais()
    {
        $this->load->model('contato/filial');
        $data['filiais'] = $this->filial->get_all();
        $this->load->view('contato/filiais', $data);
    }

    function ajax_check() 
    {
        $this->load->library('form_validation');
        $this->load->library('typography');

        if($this->input->post('ajax') == '1') 
        {
            $this->form_validation->set_rules('nome', 'nome', 'required|xss_clean|max_length[255]');
            $this->form_validation->set_rules('email', 'email', 'trim|valid_email|required|xss_clean');
            $this->form_validation->set_rules('telefone', 'telefone', 'required|xss_clean|max_length[18]');
            $this->form_validation->set_rules('mensagem', 'mensagem', 'required|xss_clean|min_length[10]');
            if($this->form_validation->run() == FALSE) 
            {
                    echo validation_errors();
            } 
            else 
            {
                    // send an email  
                    $this->load->library('email');  
                    $this->email->from($this->input->post('email'),$this->input->post('nome'));  
                    $this->email->to("contato@kufa.adv.br");
                    $this->email->bcc('nilton@trupe.net'); 
                    $this->email->subject('Contato do site Kufa - ' . $this->input->post('nome'));
                    $data['dados'] = array(
                                    'nome' => $this->input->post('nome'),
                                    'email' => $this->input->post('email'),
                                    'telefone' => $this->input->post('telefone'),
                                    'mensagem' => $this->typography->auto_typography($this->input->post('mensagem')),
                            );
                    $email_view = $this->load->view('contato/email', $data, TRUE);

                    $this->email->message($email_view);

                    if($this->email->send())
                    {
                            echo 'Mensagem enviada com sucesso!';
                    } 
                    else
                    {
                            echo $this->email->print_debugger();
                            echo 'Erro ao enviar mensagem, tente novamente.';
                    }
            }
        }
    }
}

