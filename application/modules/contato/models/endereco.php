<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Endereço
*
*
* @author Nilton de Freitas
* @link http://trupe.net
*
*/
Class Endereco extends Datamapper{
    var $table = 'enderecos';

     public function __construct()
    {
        // model constructor
        parent::__construct();
    }

    function get_endereco()
    {
        $endereco = new Endereco();
        $endereco->where('id', '1');
        $endereco->limit(1);
        $endereco->get();

        return $endereco;
    }

    function change($dados)
    {
        $endereco = new Endereco();
        $endereco->where('id', $dados['id'])->get();

        $update_data = array();

        foreach($dados as $chave => $valor)
        {
            $update_data[$chave] = $valor;
        }

        $update = $endereco->update($update_data);
        return $update;
    }

}