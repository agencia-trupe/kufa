<?php
/**
* File: filial.php
* 
* PHP version 5.3
*
* @category Kufa
* @package  Kufa
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Filial
 * 
 * @category Kufa
 * @package  Models
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/

Class Filial extends Datamapper{
    var $table = 'filiais';

    /**
     * Retorna todos os registros do banco de dados
     * 
     * @return array registros
     */
    function get_all()
    {
        $filial = new Filial();
        $filial->order_by('ordem', 'ASC')->get();
        $result = array();
        foreach($filial->all as $filial)
        {
            $result[] = $filial;
        }
        if(sizeof($result))
        {
            return $result;
        }
        return FALSE;
    }

    function get_filial()
    {
        $correspondente = new Correspondente();
        $correspondente->where('id', '1');
        $correspondente->limit(1);
        $correspondente->get();

        return $correspondente;
    }

    /**
     * Obtem os dados de um registros baseado em um valor e tipo
     * 
     * @param mixed  $valor parâmetro para busca
     * @param string $tipo  tipo de parâmetro.
     * 
     * @return object objeto
     */
    function get_conteudo( $valor, $tipo )
    {
        $filial = new Filial();
        $filial->where( $tipo, $valor )->get();
        if( $filial->exists() ){
            return $filial;
        }
        return NULL;
    }

    /**
     * Atualiza as informações de um registro
     * 
     * @param array $dados dados para atualização.
     * 
     * @return array informações atualizadas
     */
    function change($dados)
    {
        $filial = new Filial();
        $filial->where('id', $dados['id']);
        $update_data = array();
        foreach ($dados as $chave => $valor)
        {
            $update_data[$chave] = $valor;
        }
        $update_data['updated'] = time();
        $update = $filial->update($update_data);
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Insere um novo registro no banco
     * 
     * @param array $dados dados do registro.
     * 
     * @return boolean status
     */
    function insert($dados)
    {
        $serviços = new Filial();
        $serviços->get();
        $count = $serviços->result_count();

        $filial = new Filial();
        foreach ($dados as $chave => $valor)
        {
            $filial->$chave = $valor;
        }
        $filial->created = time();
        $filial->ordem = $count;
        $insert = $filial->save();
        if($insert)
        {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Remove um registro do banco de dados
     * 
     * @param int $parceiro_id id do serviço.
     * 
     * @return boolean status
     */
    function apaga($parceiro_id)
    {
        $filial = new Filial();
        $filial->where('id', $parceiro_id)->get();
        if($filial->delete())
        {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Ordena os dados para exibição
     * 
     * @param array $dados array com dados para ordenar
     * 
     * @return boolean status
     */
    function ordena($dados)
    {
        $result = array();
        foreach($dados as $chave => $valor)
        {
            $filial = new Filial();
            $filial->where('id', $valor);
            $update_data = array(
                'ordem' => $chave
                );
            if($filial->update($update_data))
            {
                $result[] = $valor;
            }
        }
        if(sizeof($result))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}

/* End of file filial.php */
/* Location: ./modules/filiais/models/filial.php */