<div class="row-fluid">
    <div class="span9">

    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?>
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <legend>Editar contatos</legend>
    <?=form_open('contato/admin_contatos/processa'); ?>
    <?=form_hidden( 'id', $contato->id ); ?>
    <?=form_hidden( 'mostrar_end', 'false' ); ?>
    <div class="checkbox" style="margin-bottom: 20px;">
    <label>
    <?=form_checkbox(array(
        'name' => 'mostrar_end',
        'value' => 'true',
        'checked' => ($contato->mostrar_end == 'true' ? TRUE : FALSE)
    )); ?> Mostrar Endereço
    </label>
    </div>
    <?=form_label('Rua'); ?>
    <?=form_input(array(
        'name' => 'rua',
        'value' => set_value('rua', $contato->rua),
        'class' => 'span7'
    )); ?>
    <?=form_error('rua'); ?>
    <?=form_label('Bairro'); ?>
    <?=form_input(array(
        'name' => 'bairro',
        'value' => set_value('bairro', $contato->bairro),
        'class' => 'span3'
    )); ?>
    <?=form_error('bairro'); ?>
    <?=form_label('Cidade'); ?>
    <?=form_input(array(
        'name' => 'cidade',
        'value' => set_value('cidade', $contato->cidade),
        'class' => 'span3'
    )); ?>
    <?=form_error('cidade'); ?>
    <?=form_label('UF'); ?>
    <?=form_input(array(
        'name' => 'uf',
        'value' => set_value('uf', $contato->uf),
        'class' => 'span1'
    )); ?>
    <?=form_error('uf'); ?>
    <?=form_label('CEP'); ?>
    <?=form_input(array(
        'name' => 'cep',
        'value' => set_value('cep', $contato->cep),
        'class' => 'span2'
    )); ?>
    <?=form_error('cep'); ?>
    <?=form_label('Email'); ?>
    <?=form_input(array(
        'name' => 'email',
        'value' => set_value('email', $contato->email),
        'class' => 'span4'
    )); ?>
    <?=form_error('email'); ?>

    <?=form_label('Telefone'); ?>
    <?=form_input(array(
        'name' => 'telefone',
        'value' => set_value('telefone', $contato->telefone),
        'class' => 'span3'
    )); ?>
    <?=form_error('telefone'); ?>

    <?=form_label('Facebook'); ?>
    <?=form_input(array(
        'name' => 'facebook',
        'value' => set_value('facebook', $contato->facebook),
        'class' => 'span8'
    )); ?>
    <?=form_error('facebook'); ?>

    <?=form_label('Google Maps API'); ?>
    <?=form_textarea(array(
        'name' => 'googlemaps',
        'value' => set_value('googlemaps', $contato->googlemaps),
        'class' => 'span8'
    )); ?>
    <?=form_error('googlemaps'); ?>

    <br>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('contatos/admin_contatos/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
    </div>
</div>