<div class="row-fluid">
    <div class="span9">
        <?php if(isset($error)): ?>
    <div class="alert alert-error">
        <?php echo $error; ?>
    </div>
    <?php endif; ?> 
       <legend><?=($acao == 'editar') ? 'Editar' : 'Cadastrar'; ?> Filial</legend>
    <?php 
            switch ($acao) {
                case 'editar':
                    $action = 'painel/filiais/edit';
                    break;
                
                default:
                    $action = 'painel/filiais/add';
                    break;
            }
    ?>
    <?=form_open_multipart($action); ?>
    <?=form_hidden( 'id', ( $acao == 'editar') ? $filial->id : '' ); ?>
    <?=form_label('Local'); ?>
    <?=form_input(array(
        'name' => 'locale',
        'value' => set_value('locale', ( $acao == 'editar') ? $filial->local : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('locale'); ?>

    <?=form_label('Contato'); ?>
    <?=form_input(array(
        'name' => 'contato',
        'value' => set_value('contato', ( $acao == 'editar') ? $filial->contato : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('contato'); ?>

    <?=form_label('Telefone'); ?>
    <?=form_input(array(
        'name' => 'telefone',
        'value' => set_value('telefone', ( $acao == 'editar') ? $filial->telefone : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('telefone'); ?>

    <?=form_label('Local'); ?>
    <?=form_input(array(
        'name' => 'local',
        'value' => set_value('local', ( $acao == 'editar') ? $filial->local : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('local'); ?>

    <?=form_label('Endereço'); ?>
    <?=form_input(array(
        'name' => 'endereco',
        'value' => set_value('endereco', ( $acao == 'editar') ? $filial->endereco : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('endereco'); ?>

    <?=form_label('Número'); ?>
    <?=form_input(array(
        'name' => 'numero',
        'value' => set_value('numero', ( $acao == 'editar') ? $filial->numero : ''),
        'class' => 'span1'
    )); ?>
    <?=form_error('numero'); ?>

    <?=form_label('Complemento'); ?>
    <?=form_input(array(
        'name' => 'complemento',
        'value' => set_value('complemento', ( $acao == 'editar') ? $filial->complemento : ''),
        'class' => 'span4'
    )); ?>
    <?=form_error('complemento'); ?>

    <?=form_label('Bairro'); ?>
    <?=form_input(array(
        'name' => 'bairro',
        'value' => set_value('bairro', ( $acao == 'editar') ? $filial->bairro : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('bairro'); ?>

    <?=form_label('Cidade'); ?>
    <?=form_input(array(
        'name' => 'cidade',
        'value' => set_value('cidade', ( $acao == 'editar') ? $filial->cidade : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('cidade'); ?>

    <?=form_label('UF'); ?>
    <?=form_input(array(
        'name' => 'uf',
        'value' => set_value('uf', ( $acao == 'editar') ? $filial->uf : ''),
        'class' => 'span1'
    )); ?>
    <?=form_error('uf'); ?>

    <?=form_label('CEP'); ?>
    <?=form_input(array(
        'name' => 'cep',
        'value' => set_value('cep', ( $acao == 'editar') ? $filial->cep : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('cep'); ?>
    
    <br>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('painel/filiais', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?> 
    </div>
</div>