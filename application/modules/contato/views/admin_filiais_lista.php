<div class="row-fluid">
    <div class="span9">
         <legend>Filiais <a class="btn btn-info btn-mini" href="<?=site_url('painel/filiais/add'); ?>">Novo</a></legend>
         <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Local</th><th>Contato</th><th class="span2"><i class="icon-cog"></i></th>
            </tr>
        </thead>
        <tbody>
        <?php if($filiais): ?>
            <?php foreach ($filiais as $filial): ?>
                <tr>
                    <td><?php echo $filial->local ?></td>
                    <td><?=$filial->contato; ?></td>
                    <td>
                        <?=anchor('painel/filiais/edit/' . $filial->id, 'Editar', 'class="btn btn-mini btn-warning"'); ?>
                        <?php echo form_open('painel/filiais/delete', 'style="display:inline"') ?>
                            <?php echo form_hidden('id', $filial->id) ?>
                            <?php echo form_submit('', 'apagar', 'class="btn btn-mini btn-danger"') ?>
                        <?php echo form_close() ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
    </table>
    </div>
</div>