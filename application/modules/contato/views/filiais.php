<h1>Filiais</h1>
<?php foreach ($filiais as $filial): ?>
	<p>
		<?php echo $filial->local; ?><br>
		Contato: <?php echo $filial->contato ?><br>
		<?php echo $filial->telefone ?><br>
		<?php echo $filial->endereco; ?>,<br>
		nº <?php echo $filial->numero ?>, <?php echo $filial->complemento ?>, <?php echo $filial->bairro ?>,<br>
		CEP: <?php echo $filial->cep ?>, <?php echo $filial->cidade ?>/<?php echo $filial->uf ?>
	</p>
<?php endforeach ?>