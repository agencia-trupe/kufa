<h1 class="page">Contato</h1>
		<?php if ($contato->mostrar_end == 'true'): ?>
		<div class="left mostrar_end">
			<div id="endereco-contato">
				<?php echo Modules::run('contato/endereco', 'contato'); ?>
			</div>
			<div class="left frame-contato">
				<iframe width="480" height="274" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?php echo $contato->googlemaps; ?>"></iframe>
			</div>
			<div class="clearfix"></div>
		</div>
		<?php endif; ?>
		<div class="left form">
			<div id="endereco-mail">
				<?php echo Modules::run('contato/endereco', 'mail'); ?>
			</div>
			<div id="ajax-loader">
					<img src="<?php echo base_url(); ?>assets/img/loader.gif" alt="">
				</div>
			<?php echo form_open(); ?>
				<?=form_input(array('name'=>'nome','value'=>'','class'=>'nome textbox', 'placeholder'=>'Nome', 'title'=>'Nome'))?><br />
				<?=form_input(array('name'=>'telefone','value'=>'','class'=>'telefone textbox', 'placeholder'=>'Telefone', 'title'=>'Telefone'))?>
				<?=form_input(array('name'=>'email','value'=>'','class'=>'email textbox', 'placeholder'=>'Email', 'title'=>'Email'))?><br>
				<?=form_textarea(array('name'=>'mensagem','value'=>'','class'=>'mensagem textbox', 'placeholder'=>'Mensagem', 'title'=>'mensagem'))?><br />
				<input type="submit" name="submit" value="enviar &raquo;" id="submit">
				<?=form_close("\n")?>
				<div id="message">
				</div>
		</div>
		<div class="clearfix"></div>