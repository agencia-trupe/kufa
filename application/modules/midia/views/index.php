<h1 class="page">Na Mídia</h1>
<div class="sidebar midia">
<div class="imprensa">
    <div class="imprensa-imagem">
        <img src="<?php echo base_url('assets/img/imprensa.jpg') ?>" alt="Kufa, sociedade de advogados - Assessoria de Imprensa">
    </div>
</div>
</div>
<div class="content">
    <div class="text">
       <?php foreach($results as $result): ?>
        <article>
        <div class="conteudo">
            <div class="data">
                <p>
                    <?php echo date('d', $result->data_publicacao) . ' de ' 
                    .$this->calendar->get_month_name(date('m', $result->data_publicacao))
                    .' de ' . date('Y', $result->data_publicacao); ?>
                </p>
            </div>
            <h1><a href="<?php echo site_url('midia/post/' . $result->id); ?>">
                <?php echo $result->titulo; ?></a></h1>
            <p><?php echo substr(strip_tags($result->conteudo), 0, 200); ?>...</p>
            <?php if ($result->arquivo): ?>
                <a href="<?php echo site_url('assets/arquivos/' . $result->arquivo) ?>" class="noticia-arquivo">Download</a>
            <?php endif ?>
        </div>
        <div class="clearfix"></div>
        </article>
    <?php endforeach; ?>
<?php echo $this->pagination->create_links(); ?>
    </div>
</div>
<div class="clearfix"></div>