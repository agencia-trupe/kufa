<h1 class="page">Na Mídia</h1>
<div class="sidebar midia">
<div class="imprensa">
    <div class="imprensa-imagem">
        <img src="<?php echo base_url('assets/img/imprensa.jpg') ?>" alt="Kufa, sociedade de advogados - Assessoria de Imprensa">
    </div>
</div>
</div>
<div class="content">
    <div class="text post">
        <article>
        <div class="conteudo">
            <div class="data">
                <p>
                    <?php echo date('d', $post->data_publicacao) . ' de ' 
                    .$this->calendar->get_month_name(date('m', $post->data_publicacao))
                    .' de ' . date('Y', $post->data_publicacao); ?>
                </p>
            </div>
            <h1><?php echo $post->titulo; ?></h1>
            <p class="resumo">
                <?php echo $post->resumo; ?>
            </p>
            <?php if($post->imagem != NULL): ?>
                <a href="<?php echo base_url(); ?>assets/img/noticias/<?php echo $post->imagem; ?>" class="noticia-img">
                    <img src="<?php echo base_url(); ?>assets/img/noticias/thumbs/<?php echo $post->imagem; ?>" alt="<?php echo $post->titulo; ?>">
                </a>
            <?php endif; ?>

            <?php if($post->video != NULL): ?>
                <?php echo $post->video ?>
            <?php endif; ?>

            <?php echo $post->conteudo; ?>
        </div>
        <div class="clearfix"></div>
        <a href="<?php echo site_url('midia'); ?>" class="voltar">Voltar</a>
        </article>
    </div>
</div>
<div class="clearfix"></div>