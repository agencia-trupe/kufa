<?php
/**
* File: midia.php
* 
* PHP version 5.3
*
* @category Kufa
* @package  Kufa
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Midia
 * 
 * @category Kufa
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/

class Midia extends MX_Controller
{
	var $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('noticias/noticia');
		$this->data['pagina'] = 'midia';
		$this->data['slug'] = 'midia';
	}

	public function lista()
    {      
        $this->load->library('calendar');
        $this->load->library('pagination');
            $pagination_config = array(
                        'base_url'       => site_url() . 'midia/',
                        'total_rows'     => $this->db->where(array(
                                                'data_publicacao <=' => time(),
                                                'categoria' => 'midia')
                                            )->get('noticias')->num_rows(),
                        'per_page'       => 1,
                        'num_links'      => 5,
                        'next_link'      => 'próximo',
                        'prev_link'      => 'anterior',
                        'first_link'     => FALSE,
                        'last_link'      => FALSE, 
                        'full_tag_open'  => '<div class="pagination center"><ul>',
                        'full_tag_close' => '</ul></div>',
                        'cur_tag_open'   => '<li class="active"><a href="#">',
                        'cur_tag_close'  => '</a></li>',
                        'num_tag_open'   => '<li>',
                        'num_tag_close'  => '</li>',
                        'next_tag_open'   => '<li>',
                        'next_tag_close'  => '</li>',
                        'prev_tag_open'   => '<li>',
                        'prev_tag_close'  => '</li>',
                        'uri_segment'	=>	2
                );
            $this->pagination->initialize($pagination_config);
            //Obtendo resultados no banco
            $this->data['results'] = $this->noticia->get_all('midia', $pagination_config['per_page'], $this->uri->segment(2));

       	$this->data['titulo'] = 'Na Mídia';
        $this->data['conteudo_principal'] = "midia/index";
        $this->load->view('layout/template', $this->data);
    }

    public function post($id)
    {
        $this->load->library('calendar');
        $this->load->model('noticias/noticia');
        $data['post'] = $this->noticia->get_noticia($id);
        $data['relateds'] = $this->noticia->get_relate($id);
        $anterior = $this->noticia->get_anterior($id);
        $data['anterior'] = $anterior ? $anterior->id : NULL;
        $proximo = $this->noticia->get_proximo($id);
        $data['proximo'] = $proximo ? $proximo->id : NULL;
        $data['titulo'] = 'Kufa &middot; Na Mídia';
        $data['slug'] = 'midia';
        $data['conteudo_principal'] = "midia/post";
        $this->load->view('layout/template', $data);
    }
}