<div class="conteudo conteudo-responsabilidade">
	<div class="clearfix"></div>
	<div class="header">
		<h1>Responsabilidade Social</h1>
	</div>
	<div class="texto">
		<p> A Meta RH apoia projetos sociais junto a diversas entidades, tais como: </p>
		<div class="separador"></div>

		<!--Doutores da Alegria-->
		<div class="imagem left">
			<img src="<?php echo base_url(); ?>assets/img/doutores-alegria.png" alt="Dotores da alegria">
		</div>

		<div class="descricao right">
			<h3>Doutores da Alegria</h4>
	 		<p>
	 			Seus artistas, conhecidos como besteirologistas, em duplas, visitam 
	 			crianças hospitalizadas, leito a leito, duas vezes por semana, 
	 			durante aproximadamente seis horas por dia, inclusive nas unidades 
	 			de terapia intensiva e de procedimentos ambulatoriais. A cada seis 
	 			meses os artistas se revezam entre os hospitais. 
	 		</p>
		</div>
		
		<div class="clearfix"></div>
		<div class="separador"></div>
		<!--Fim Doutores da Alegria-->

		<!--Fundação ABRINQ-->
		<div class="imagem left">
			<img src="<?php echo base_url(); ?>assets/img/abrinq.png" alt="Fundação ABRINQ" 
			title="Fundação ABRINQ">
		</div>

		<div class="descricao right">
			<h3>Fundação ABRINQ</h3> 
			<p>
				Somos uma organização 'amiga da criança', sem fins lucrativos, 
				que nasceu em 1990, ano da promulgação do Estatuto da Criança e 
				do Adolescente. Atualmente, a Fundação Abrinq beneficia milhões 
				de crianças e adolescentes por meio de suas ações, seus programas 
				e projetos e com a ajuda de muitos parceiros. 
			</p>
		</div>

		<div class="clearfix"></div>
		<div class="separador"></div>
		<!--Fim Fundação ABRINQ-->

		<!--Instituto Cultural Gotas de Flor com Amor-->
		<div class="imagem left">
			<img src="<?php echo base_url(); ?>assets/img/gotas-de-flor.png" alt="Instituto Cultural Gotas de Flor com Amor" 
			title="Instituto Cultural Gotas de Flor com Amor">
		</div>

		<div class="descricao right">
			<h3>Instituto Cultural Gotas de Flor com Amor</h3> 
			<p>
				Instituição que, atualmente, conta com cerca de 130 voluntários que trazem 
				sua contribuição para juntos conseguirmos fazer um Terceiro Setor, forte, 
				atuante, com qualidade, transparente, conseguindo como resultado uma criança 
				e um jovem mais feliz, colocado profissionalmente, com uma visão de futuro, 
				podendo ter seus próprios sonhos e conseguir correr atrás de sua realização. 
			</p>
		</div>

		<div class="clearfix"></div>
		<div class="separador"></div>
		<!--Fim Instituto Cultural Gotas de Flor com Amor-->


	</div>
	<div class="clearfix"></div>
</div>