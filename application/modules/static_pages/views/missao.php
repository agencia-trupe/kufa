<div class="conteudo conteudo-missao">
	<div class="clearfix"></div>
	<div class="header">
		<h1>Visão, Missão, Valores</h1>
	</div>
	<div class="texto">
		<h2>Visão</h2>
		<p>
			Manter reputação idônea e estabelecer relações de longo prazo baseada em ética,
			 confiança mútua, integridade, repeito e transparência com nossos colaboradores, 
			 clientes, fornecedores, parceiros e a comunidade.
		</p>
		<div class="separador"></div>

		<h2>Missão</h2>
		<p>
			Identificar competências que se compatibilizem aos valores e princípios de nossos 
			clientes. Acreditamos que pessoas certas com valores compatíveis aos da empresa, 
			geram engajamento e efetividade em sua entrega.
		</p>
		<div class="separador"></div>

		<h2>Valores</h2>
		<p>
			Ética, respeito à diversidade, transparência, perenidade nas relações e busca de 
			objetivos compartilhados e sinérgicos.
		</p>
		<div class="separador"></div>
	</div>
	<div class="clearfix"></div>
</div>