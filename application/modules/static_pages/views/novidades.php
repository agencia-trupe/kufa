<div class="conteudo-novidades">
    <div class="interna">
        <h1>Novidades | Artigos</h1>
        <div class="clearfix"></div>
        <article>
            <a href="noticias/post/1" class="thumb">
                <img src="<?php echo base_url(); ?>assets/img/clientes/10.jpg" alt="Miniatura Notícia 1">
            </a>
            <div class="conteudo">
                <h1>Lorem ipsum dolor</h1>
                <p><em><b>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</b></em></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Quisque posuere purus eu nisl imperdiet ultrices. Vivamus 
                    elementum interdum velit, sed egestas lacus rhoncus ac. 
                    Cras sodales feugiat massa, vel lacinia eros convallis sit amet. 
                </p>
            </div>
            <div class="data"><b>12/10/2012</b></div>
            <div class="clearfix"></div>
            <div class="separador"></div>
            <div class="more">
                <a href="novidades/post/1">Continuar lendo &raquo;</a>
            </div>
        </article>
        <article>
            <div class="conteudo">
                <h1>Lorem ipsum dolor</h1>
                <p><em><b>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</b></em></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Quisque posuere purus eu nisl imperdiet ultrices. Vivamus 
                    elementum interdum velit, sed egestas lacus rhoncus ac. 
                    Cras sodales feugiat massa, vel lacinia eros convallis sit amet. ,
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Quisque posuere purus eu nisl imperdiet ultrices. Vivamus 
                </p>
            </div>
            <div class="data"><b>12/10/2012</b></div>
            <div class="clearfix"></div>
            <div class="separador"></div>
            <div class="more">
                <a href="novidades/post/1">Continuar lendo &raquo;</a>
            </div>
        </article>
        <article>
            <a href="noticias/post/1" class="thumb">
                <img src="<?php echo base_url(); ?>assets/img/clientes/11.jpg" alt="Miniatura Notícia 1">
            </a>
            <div class="conteudo">
                <h1>Lorem ipsum dolor</h1>
                <p><em><b>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</b></em></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Quisque posuere purus eu nisl imperdiet ultrices. Vivamus 
                    elementum interdum velit, sed egestas lacus rhoncus ac. 
                    Cras sodales feugiat massa, vel lacinia eros convallis sit amet. 
                </p>
            </div>
            <div class="data"><b>12/10/2012</b></div>
            <div class="clearfix"></div>
            <div class="separador"></div>
            <div class="more">
                <a href="novidades/post/1">Continuar lendo &raquo;</a>
            </div>
        </article>
        <article>
            <a href="noticias/post/1" class="thumb">
                <img src="<?php echo base_url(); ?>assets/img/clientes/12.jpg" alt="Miniatura Notícia 1">
            </a>
            <div class="conteudo">
                <h1>Lorem ipsum dolor</h1>
                <p><em><b>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</b></em></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Quisque posuere purus eu nisl imperdiet ultrices. Vivamus 
                    elementum interdum velit, sed egestas lacus rhoncus ac. 
                    Cras sodales feugiat massa, vel lacinia eros convallis sit amet. 
                </p>
            </div>
            <div class="data"><b>12/10/2012</b></div>
            <div class="clearfix"></div>
            <div class="separador"></div>
            <div class="more">
                <a href="novidades/post/1">Continuar lendo &raquo;</a>
            </div>
        </article>
        <article>
            <a href="noticias/post/1" class="thumb">
                <img src="<?php echo base_url(); ?>assets/img/clientes/25.jpg" alt="Miniatura Notícia 1">
            </a>
            <div class="conteudo">
                <h1>Lorem ipsum dolor</h1>
                <p><em><b>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</b></em></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Quisque posuere purus eu nisl imperdiet ultrices. Vivamus 
                    elementum interdum velit, sed egestas lacus rhoncus ac. 
                    Cras sodales feugiat massa, vel lacinia eros convallis sit amet. 
                </p>
            </div>
            <div class="data"><b>12/10/2012</b></div>
            <div class="clearfix"></div>
            <div class="separador"></div>
            <div class="more">
                <a href="novidades/post/1">Continuar lendo &raquo;</a>
            </div>
        </article>
    </div>
</div>