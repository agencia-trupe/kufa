<div class="banners-wrapper">
	<div id="banner">
		<?php foreach ($slides as $slide): ?>
				<a href="<?php echo $slide->link ?>" data-titulo="<?php echo $slide->titulo ?>">
					<img src="<?php echo base_url(); ?>assets/img/slides/<?php echo $slide->imagem ?>" alt="<?php echo $slide->titulo ?>">
				</a>
		<?php endforeach ?>
	</div>
	<div id="banner-texto">
		<a id="slide-titulo"></a>
		<a class="control" id="control-prev" href="#">anterior</a>
		<a class="control" id="control-next" href="#">próximo</a>
	</div>
</div>
<div class="clearfix"></div>
<?php echo Modules::run('noticias/home'); ?>
<div class="chamadas">
	<div class="chamada" id="chamada-equipe">
		<h1>Equipe</h1>
		<p>
			<?=$chamadas->equipe?>
		</p>
		<a id="more" href="<?php echo site_url('equipe'); ?>">saiba mais &raquo;</a>
	</div>
	<div class="chamada" id="chamada-atuacao">
		<h1>Áreas de atuação</h1>
		<p>
			<?=$chamadas->atuacao?>
		</p>
		<a id="more" href="<?php echo site_url('atuacao'); ?>">saiba mais &raquo;</a>
	</div>
	<div class="clearfix"></div>
	<div class="separador-horizontal"></div>
	<div class="eventos">
		<h1>Próximos Eventos</h1>
		<?php echo Modules::run('eventos/widget'); ?>
	</div>
</div>
<div class="separador-vertical"></div>
<div class="boletins">
	<h1>Boletins informativos</h1>
	<span>Cadastre-se para receber</span>
	<?php echo form_open('', 'id="boletins"') ?>
	<div class="inputs">
		<?php echo form_input(array('name'=>'nome', 'class'=>'boletim_nome', 'placeholder' => 'Nome', 'required'=>'required')) ?>
		<?php echo form_input(array('name'=>'email', 'type'=>'email', 'class'=>'boletim_email', 'placeholder' => 'Email', 'required'=>'required')) ?>
	</div>
	<?php echo form_submit('', '') ?>
</div>
<a href="http://www.ipade.org.br" target="_blank" class="ipade">
	<span class="cinza">Conheça o IPADE »</span>
</a>
<div class="clearfix"></div>
