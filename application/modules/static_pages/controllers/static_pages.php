<?php

Class Static_pages extends MX_Controller
{

        public function index()
        {
                $this->home();
        }

        public function home()
        {
        	$data['titulo'] = 'Home';
                $data['slug'] = 'home';
                $data['pagina'] = 'home';
                $this->load->model('admin_slideshow/slide');
                $data['slides'] = $this->slide->get_all();
                $this->load->model('paginas/pagina');
                $data['chamadas'] = $this->pagina->get_chamadas('apresentacao');
                $data['conteudo_principal'] = "static_pages/home";
                $this->load->view('layout/template', $data);
        }

        public function perfil()
        {
                $data['titulo'] = 'Kufa &middot; Perfil';
                $data['pagina'] = 'perfil';
                $data['conteudo_principal'] = "static_pages/perfil";
                $this->load->view('layout/main', $data);
        }

        public function empresas()
        {
                $data['titulo'] = 'Kufa &middot; Para Empresas';
                $data['pagina'] = 'empresas';
                $data['conteudo_principal'] = "static_pages/empresas";
                $this->load->view('layout/main', $data);
        }

        public function clientes()
        {
                $data['titulo'] = 'Kufa &middot; Cases | Clientes';
                $data['pagina'] = 'clientes';
                $data['conteudo_principal'] = "static_pages/clientes";
                $this->load->view('layout/main', $data);
        }

        /*public function novidades()
        {
                $data['titulo'] = 'Kufa &middot; Novidades | Artigos';
                $data['pagina'] = 'novidades';
                $data['conteudo_principal'] = "static_pages/novidades";
                $this->load->view('layout/main', $data);
        }*/

}
