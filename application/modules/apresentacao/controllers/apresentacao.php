<?php

Class Apresentacao extends MX_Controller
{
    private $titulo;
    private $description;
    private $slug;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('paginas/pagina');
        $this->titulo = 'Apresentação';
        $this->slug = 'apresentacao';
        $this->description = $this->pagina->get_resumo($this->slug);
        $this->load->model('fotos/foto');
    }

    public function index()
    {
        //conteudo do banco de dados
        $data['slug']      = $this->slug;
        $data['conteudo']      = $this->pagina->get_conteudo($this->slug);

        $data['fotos'] = $this->foto->get_all(null, null);

        //metas
        $data['titulo']      = $this->titulo;
        $data['description'] = $this->description;
        //layout
        $data['conteudo_principal'] = 'apresentacao/apresentacao_view';
        $this->load->view('layout/template', $data);
    }
}