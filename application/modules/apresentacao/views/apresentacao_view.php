<h1 class="page"><?php echo $conteudo->titulo; ?></h1>
<div class="content">
    <?php echo $conteudo->texto; ?>
    <div class="thumbs">
    	<?php foreach($fotos as $foto): ?>
            <a data-caption="Teste de legenda" class="galeria" rel="prettyPhoto['fotos']" href="<?php echo base_url(); ?>assets/img/fotos/display/<?php echo $foto->imagem; ?>" 
                 title="<?php echo $foto->descricao; ?>">
                <img width="178" id="thumb" src="<?php echo base_url(); ?>assets/img/fotos/thumbs/<?php echo $foto->imagem; ?>" alt="<?php echo $foto->titulo; ?>">
                <h1><?php echo $foto->titulo; ?></h1>
            </a>
        <?php endforeach; ?>
        <div class="clearfix"></div>
    </div>
</div>
<!-- <div class="sidebar">
    <?php echo Modules::run('contato/filiais'); ?>
   <?php echo Modules::run('parceiros/apresentacao'); ?>
</div> -->
<div class="clearfix"></div>
