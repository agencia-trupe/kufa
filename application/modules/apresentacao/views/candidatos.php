<div class="conteudo-candidatos">
	<div class="interna">
		<h1>Para candidatos</h1>
		<div class="descricao">
			<p>
				<b>A Kufa</b>
				É uma empresa especializada em Programas de Trainees, Estagiários e Menores Aprendizes.</p>

			<p>
				Selecionamos jovens com potencial, oferecendo oportunidades de 
				desenvolvimento e crescimento em empresas renomadas, que os 
				preparam para assumir no futuro, posições desafiadoras. 
			</p>
		</div>
		<div class="grupo">
			<div class="texto">
				<p id="vermelho">
					<span>Selecione abaixo</span> a opção<br> que mais se enquadra em <span>seu perfil</span>
				</p>
				<p>
					E acesse as melhores vagas do mercado
				</p>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="interna">
		<div id="boxes">
			<div class="box" id="trainees" >
				<div class="conteudo-box">
					<h1>Trainees</h1>
					<p>Acompanhe aqui os Programas para Trainees em andamento.</p>
					<a href="<?php echo base_url(); ?>programas/trainees" >Programas <div class="seta"></div></a>
					<a href="<?php echo base_url(); ?>vagas/trainees" >Vagas <div class="seta"></div></a>
				</div>
			</div>
			<div class="box" id="estagios" >
				<div class="conteudo-box">
					<h1>Estágios</h1>
					<p>Diversas oportunidades para quem <br>está iniciando a carreira profissional. </p>
					<a href="<?php echo base_url(); ?>programas/estagios" >Programas <div class="seta"></div></a>
					<a href="<?php echo base_url(); ?>vagas/trainees" >Vagas <div class="seta"></div></a>
				</div>
			</div>
			<div class="box ultimo" id="aprendizes">
				<div class="conteudo-box">
					<h1>Aprendizes</h1>
					<p>Menores aprendizes podem conseguir seu primeiro emprego aqui.</p>
					<a href="<?php echo base_url(); ?>programas/aprendizes" >Ver mais <div class="seta"></div></a>
				</div>				
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>