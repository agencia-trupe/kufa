<div id="banner"></div>
<div class="clearfix"></div>
<div class="chamadas">
	<div id="chamada-equipe">
		<h1>Equipe</h1>
		<p>
			Os sócios do escritório se mantém em constante estudo, desenvolvendo
			atividades acadêmicas, coordenando e ministrando palestras e cursos
		</p>
		<a href="#">saiba mais &raquo;</a>
	</div>
	<div id="chamada-atuacao">
		<h1>Áreas de atuação</h1>
		<p>
			A Kufa Sociedade de Advogados atua em diversos ramos do Direito Público,
			prestando acessoria jurídica especializada. 
		</p>
		<a href="#">saiba mais &raquo;</a>
	</div>
	<div class="separador-horizontal"></div>
	<h1>Notícias e publicações</h1>
	<?php if(isset($noticias)): ?>
		<?php foreach($noticias as $noticia): ?>
			<div class="noticia-bloco">
				<h1><?php echo $noticia->titulo; ?></h1>
				<p><?php echo strip_tags($noticia->resumo); ?> 
					<a href="<?php echo base_url() . 'noticias/detalhe/' . $noticia->id; ?>">&raquo;</a> </p>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
</div>
<div class="separador-vertical"></div>
<div class="eventos">
	<h1>Próximos eventos</h1>
	<?php if(isset($eventos)): ?>
		<?php foreach($eventos as $evento): ?>
			<div class="evento-box">
				<div class="evento-data">
					<p class="dia"><?php echo $date('d', $evento->data); ?></p>
					<p class="mes"><?php echo $date('m', $evento->data); ?></p>
				</div>
				<div class="evento-resumo">
					<p><?php echo strip_tags($noticia->resumo); ?></p>
				</div>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
</div>
