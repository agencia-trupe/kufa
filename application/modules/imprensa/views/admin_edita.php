<div class="row-fluid">
    <div class="span9">
        <?php if(isset($error)): ?>
    <div class="alert alert-error">
        <?php echo $error; ?>
    </div>
    <?php endif; ?> 
       <legend>Assessoria de Imprensa</legend>

    <?=form_open_multipart('painel/imprensa/edit'); ?>
    <?=form_hidden( 'id', ( $acao == 'editar') ? $imprensa->id : '' ); ?>
    <?=form_label('Nome'); ?>
    <?=form_input(array(
        'name' => 'nome',
        'value' => set_value('nome', ( $acao == 'editar') ? $imprensa->nome : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('nome'); ?>
    
    <?=form_label('DDD 1'); ?>
    <?=form_input(array(
        'name' => 'ddd1',
        'value' => set_value('ddd1', ( $acao == 'editar') ? $imprensa->ddd1 : ''),
        'class'	=> 'span1'
    )); ?>
    <?=form_error('ddd1'); ?>

    <?=form_label('Telefone 1'); ?>
    <?=form_input(array(
        'name' => 'telefone1',
        'value' => set_value('telefone1', ( $acao == 'editar') ? $imprensa->telefone1 : ''),
    )); ?>
    <?=form_error('telefone1'); ?>

    <?=form_label('DDD 2'); ?>
    <?=form_input(array(
        'name' => 'ddd2',
        'value' => set_value('ddd2', ( $acao == 'editar') ? $imprensa->ddd2 : ''),
        'class'	=> 'span1'
    )); ?>
    <?=form_error('ddd2'); ?>

    <?=form_label('Telefone 2'); ?>
    <?=form_input(array(
        'name' => 'telefone2',
        'value' => set_value('telefone2', ( $acao == 'editar') ? $imprensa->telefone2 : ''),
    )); ?>
    <?=form_error('telefone2'); ?>
    <br>
    <?php if(isset($imprensa->imagem)):?>
    <img width="200px" src="<?php echo base_url(); ?>assets/img/imprensa/<?php echo $imprensa->imagem; ?>" alt="" >
    <?php endif; ?>
    <div class="control-group">
        <label class="control-label" for="imagem">Alterar Imagem</label>
        <div class="controls">
            <?php echo form_upload('imagem', set_value('imagem')); ?>
            <span class="help-inline"><?php echo form_error('imagem'); ?></span>
        </div>
    </div>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('painel/imprensa', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?> 
    </div>
</div>