<?php
/**
* File: admin_parceiros.php
* 
* PHP version 5.3
*
* @category Kufa
* @package  Kufa
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Admin_imprensa
 * 
 * @category Kufa
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/

class Admin_imprensa extends AuthController
{
	/**
	 * Array de variáveis a ser passado para a view.
	 * 
	 * @var array
	 */
	var $data;

	/**
	 * Construtor
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->model('imprensa/imprensa_model', 'imprensa');
		$this->data['module'] = 'imprensa';
	}

	/**
	 * Ação principal
	 * 
	 * @return [type] [description]
	 */
	function index()
	{

		$this->edit();
	}

	
	/**
	 * Edita uma notícia.
	 *
	 * @param [int] $parceiro_id [description]
	 * 
	 * @return [mixed]     [description]
	 */
	function edit()
	{
		$post = array();
		//Define o array com os dados do $_POST;
		foreach ($_POST as $chave => $valor) 
		{
			$post[$chave] = $valor;
		}

		//Define a ação como cadastro para passar ao formulário
		$this->data['acao'] = 'editar';

		//Caso não haja post, retorna o forumlário de cadastro
		if( ! $post )
		{
			try 
			{
				$this->data['imprensa'] = $this->imprensa->get_conteudo(1, 'id');
			} 
			catch (Exception $e) 
			{
				//Retorna a página de listagem caso nenhuma página corresponda ao id fornecido
				$this->session->set_flashdata('error', $e->getMessage());
				redirect('painel/imprensa');
			}
			$this->data['acao'] = 'editar';
			$this->data['conteudo'] = 'admin_edita';
			$this->load->view('start/template', $this->data);
		}
		else
		{
			$imprensa = $this->imprensa->get_conteudo('1', 'id');

			//Define a imagem atual, que será apagada em caso de novo upload de imagem
			$old_image = $imprensa->imagem;

			//Retorna o formulário de cadastro com os erros caso não passe na
			//validação do formulário
			if( ! $this->form_validation->run('imprensa') )
				return $this->_form_error_return($this->data['acao'], NULL, '1', 'id');

			//Checa se houve envio de imagem
			if(strlen($_FILES['imagem']['name'])>0)
			{
				//Tenta persistir a imagem e retorna uma exception caso não persista
				try 
				{
					$upload = $this->_upload_image();
					$post['imagem'] = $upload['file_name'];
				} 
				catch (Exception $e) 
				{
					return $this->_form_error_return($this->data['acao'], $e->getMessage());
				}
			}

			//Tenta inserir a notícia no banco de dados e retorna uma exception caso haja erro;
			try 
			{
				$this->imprensa->change($post);
				if($upload)
					unlink($upload['file_path'] . $old_image);
				redirect('painel/imprensa');
			} 
			catch (Exception $e) 
			{
				if($upload)
					unlink($upload['full_path']);
				return $this->_form_error_return($this->data['acao'], $e->getMessage(), 1, 'id');
			}
		}
	}


	/**
	 * Retorna o formulário contendo a mensagem de erro
	 * 
	 * @param string $acao  	 [cadastro|edição]
	 * @param string $error 	 mensagem de erro
	 * @param mixed  $value 	 valor a buscar no banco de dados
	 * @param string $identifier identificador do valor passado
	 * 
	 * @return void formulário
	 */
	private function _form_error_return($acao, $error = NULL, $value = NULL, $identifier = NULL)
	{
		if( $acao === 'editar' )
			$this->data['imprensa'] = $this->imprensa->get_conteudo(1, 'id');
		
		if( $error )
			$this->data['error'] = $error;

		$this->data['module'] = 'imprensa';
		$this->data['acao'] = $acao;
		$this->data['conteudo'] = 'admin_edita';
		$this->load->view('start/template', $this->data);
	}

	/**
	 * Realiza o upload de uma imagem
	 *
	 * @throws Exception If cant do upload
	 * @return void dados de upload
	 */
	private function _upload_image()
	{
		$config['upload_path'] = './assets/img/imprensa/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '8000';
		$config['max_width']  = '3000';
		$config['max_height']  = '3000';
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('imagem') )
			throw new Exception($this->upload->display_errors(), 1);

		$this->load->library('image_moo');
        //Is only one file uploaded so it ok to use it with $uploader_response[0].
        $upload_data = $this->upload->data();
        $file_uploaded = $upload_data['full_path'];
        //Locais dos arquivos alterados
        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];

		if ( ! $this->image_moo->load($file_uploaded)
                                ->resize_crop(280,186,FALSE)
                                ->save($new_file,true) )
			throw new Exception("Erro ao redimensionar imagem", 1);
			
		return $this->upload->data();

	}
}

/* End of file admin_parceiros.php */
/* Location: ./modules/parceiros/controllers/admin_parceiros.php */