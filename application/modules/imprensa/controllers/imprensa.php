<?php
/**
* File: imprensa.php
* 
* PHP version 5.3
*
* @category Kufa
* @package  Kufa
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Imprensa
 * 
 * @category Kufa
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/

class Imprensa extends MX_Controller
{
	public function parcial()
	{
		$this->load->model('imprensa/imprensa_model', 'imprensa');
		$data['imprensa'] = $this->imprensa->get_conteudo(1, 'id');

		$this->load->view('imprensa/parcial', $data);	
	}

	public function cadastra()
	{
		$this->load->model('Imprensa_model');
		$imprensa = new Imprensa_model();
		$imprensa->nome = 'Daniel Augusto';
		$imprensa->ddd1 = 11;
		$imprensa->telefone1 = '3804-7711';
		$imprensa->ddd2 = 11;
		$imprensa->telefone2 = '98105-8900';
		if( ! $imprensa->save() )
			echo 'Erro';
		else
			echo 'Salvo!';
	}
}

/* End of file imprensa.php */
/* Location: ./modules/imprensa/controllers/imprensa.php */