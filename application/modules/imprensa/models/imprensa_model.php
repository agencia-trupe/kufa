<?php
/**
* File: imprensa.php
* 
* PHP version 5.3
*
* @category Kufa
* @package  Kufa
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Imprensa_model
 * 
 * @category Kufa
 * @package  Models
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/

class Imprensa_model extends Datamapper
{
	/**
	 * Tabela do banco de dados.
	 * 
	 * @var string
	 */
	var $table = 'imprensa';


	/**
	 * Obtem os dados de um registros baseado em um valor e tipo
	 * 
	 * @param mixed  $valor parâmetro para busca
	 * @param string $tipo  tipo de parâmetro.
	 * 
	 * @return object objeto
	 */
	function get_conteudo( $valor, $tipo )
	{
		$imprensa = new Imprensa_model();
		$imprensa->where( $tipo, $valor )->get();
		if( $imprensa->exists() ){
			return $imprensa;
		}
		return NULL;
	}

	/**
	 * Atualiza as informações de um registro
	 * 
	 * @param array $dados dados para atualização.
	 * 
	 * @return array informações atualizadas
	 */
	function change($dados)
	{
		$imprensa = new Imprensa_model();
		$imprensa->where('id', $dados['id']);
		$update_data = array();
		foreach ($dados as $chave => $valor)
		{
			$update_data[$chave] = $valor;
		}
		$update = $imprensa->update($update_data);
		if($update)
		{
			return TRUE;
		}
		return FALSE;
	}

}

/* End of file imprensa.php */
/* Location: ./modules/imprensas/models/imprensa.php */