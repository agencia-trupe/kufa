<div class="span9">

<legend><?php echo $acao == 'editar' ? 'Editar Integrante' : 'Nova área de atuação' ;?></legend>
    <?php echo isset($error) ? $error['error'] : ''; ?>
    <?php echo form_open_multipart(($acao == 'editar') ? 'painel/atuacao/atualiza' : 'painel/atuacao/salva', 'class="well"'); 
    ?>

    <div class="row-fluid">
      <?php echo ($acao == 'edita') ? form_hidden('acao', 'edita') : form_hidden('acao', 'cadastra'); ?>  
    </div>
    <?php if($acao == 'editar') : ?>
      <?php echo form_hidden('id', set_value('id', $atuacao->id)); ?>
    <?php endif; ?>
  <div class="row-fluid">
      <div class="span11">
          <label for="titulo">Título</label>
              <?php echo form_input(array('name'=>'titulo', 'id' => 'titulo', 'value'=>set_value('titulo', $acao == 'editar' ? $atuacao->titulo : ''), 'class' => 'span11',)); ?>
              <?php echo form_error('titulo'); ?>
      </div>
      <div class="clearfix"></div>
      <div class="control-group">
            <label class="control-label" for="texto">Texto</label>
            <div class="controls">
              <?php echo form_textarea('texto', set_value('texto', $acao == 'editar' ? $atuacao->texto : ''), 'class="tinymce span11"'); ?>
              <span class="help-inline"><?php echo form_error('texto'); ?></span>
            </div>
      </div>
     </div>
  <?php echo form_submit('submit', ($acao == 'editar') ? 'Salvar' : 'Cadastrar', 'class="btn btn-primary"'); ?>
  <?php echo form_close(); ?> 
</div>