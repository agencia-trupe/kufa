<div class="span12">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="span6">
            <legend>Áreas de atuação <?php echo anchor('painel/atuacao/cadastra', 'Cadastrar', 'class="btn"'); ?></legend>
        </div>
    </div>
  <?php if(isset($result)): ?>
     <?php
            $tmpl = array (
                                'table_open'          => '<table class="table table-striped table-bordered table-condensed">',

                                'heading_row_start'   => '<tr>',
                                'heading_row_end'     => '</tr>',
                                'heading_cell_start'  => '<th>',
                                'heading_cell_end'    => '</th>',

                                'row_start'           => '<tr>',
                                'row_end'             => '</tr>',
                                'cell_start'          => '<td>',
                                'cell_end'            => '</td>',


                                'table_close'         => '</table>'
                        );

            $this->table->set_template($tmpl); 
            $this->table->set_heading(array('Nome', 'Detalhes'));
            foreach ($result as $item)
            {
            $titulo = $item->titulo;
            $detalhes = anchor('painel/atuacao/editar/' . $item->id, 'Editar') . ' | ' . anchor('painel/atuacao/apaga/' . $item->id, 'Remover', 'id="removelink"') ;
            $this->table->add_row(array($titulo, $detalhes));
            }
            echo $this->table->generate();

            ?> 
  <?php endif; ?>
        

</div><!--/span-->