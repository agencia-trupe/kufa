<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_apresentacao extends MX_Controller
{
    function index(){

        $this->editar();
    }
    function editar()
    {
        /**
         * Verifica se o usuário está logado para então prosseguir ou não.
         */
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Kufa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            //Verifica se o usuário tem nível de acesso permitido
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
            {

                $this->load->model('paginas/pagina');
                $data['module'] = 'apresentacao';
                $data['title'] = 'Kufa - Apresentação - Editar';

                if($this->pagina->get_conteudo('apresentacao'))
                {
                    $data['apresentacao'] = $this->pagina->get_conteudo('apresentacao');
                    $data['main_content'] = 'a_edita_view';
                    $this->load->view('includes/template', $data);
                }
                else
                {
                    $this->session->set_flashdata('error', 'A ação não pode ser
                    realizada, tente novamente ou entre em contato com o suporte');
                    redirect('apresentacaos/lista');
                }

            }
            else
            {
                Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                $this->session->set_flashdata('error', 'Erro de permissão.
                                                    Você precisa ser administrador para realizar essa ação');
                redirect();
            }
        }
    }

    function atualiza()
    {
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Kufa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
            {
                $config = array(
                    array(
                        'field' => 'texto',
                        'label' => 'texto',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'equipe',
                        'label' => 'equipe',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'atuacao',
                        'label' => 'atuacao',
                        'rules' => 'required',
                    ),
                );
                $this->load->library('form_validation');
                $this->form_validation->set_rules($config);
                $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');

                if($this->form_validation->run() == FALSE )
                {
                    $this->load->model('paginas/pagina');
                    $data['module'] = 'apresentacao';
                    $data['title'] = 'Kufa - Apresentação - Editar';

                    if($this->pagina->get_conteudo('apresentacao'))
                    {
                        $data['apresentacao'] = $this->pagina->get_conteudo('apresentacao');
                        $data['main_content'] = 'a_edita_view';
                        $this->load->view('includes/template', $data);
                    }
                    else
                    {
                        $this->session->set_flashdata('error', 'A ação não pode ser
                        realizada, tente novamente ou entre em contato com o suporte');
                        redirect('apresentacao');
                    }
                }
                else
                {


                    $this->load->model('paginas/pagina');

                    //prepara o array com os dados para enviar ao model
                    $dados = array(
                            'slug' => 'apresentacao',
                            'texto' => $this->input->post('texto'),
                            'equipe' => $this->input->post('equipe'),
                            'atuacao' => $this->input->post('atuacao'),
                        );


                        if( ! $this->pagina->atualiza($dados))
                        {
                            $this->session->set_flashdata('error', 'A ação não pode ser
                            realizada, tente novamente ou entre em contato com o suporte');
                            redirect('painel/apresentacao/');
                        }
                        else
                        {
                            $this->session->set_flashdata('success', 'apresentacao atualizado
                            com sucesso!');
                            redirect('painel/apresentacao');
                        }

                }

              }
              else
              {
              Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
              $this->session->set_flashdata('error', 'Erro de permissão.
                                                    Você precisa ser administrador para realizar essa ação');
              redirect();

              }
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
