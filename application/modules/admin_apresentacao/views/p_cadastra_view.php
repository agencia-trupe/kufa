<div class="span9">
 
<legend><?php echo $acao == 'editar' ? 'Editar Programa' : 'Novo Programa' ;?></legend>
    <?php echo isset($error) ? $error['error'] : ''; ?>
    <?php
    
    $categoria = array(
        'aprendizes' => 'Aprendizes',
        'estagios' => 'Estágios',
        'trainees' => 'Trainees',
    );
    $empresa = array('name'=>'empresa', 'id' => 'empresa', 'value'=>set_value('empresa', $acao == 'editar' ? $programa->programa_empresa : ''), 'class' => 'span11',);
    
    echo form_open_multipart(($acao == 'editar') ? 'painel/programas/atualiza' : 'painel/programas/salva', 'class="well"'); ?>

    <div class="row-fluid">
        </div>
    <?php if($acao == 'editar') : ?>
    <?php echo form_hidden('id', set_value('id', $programa->id)); ?>
  <?php endif; ?>
    <div class="row-fluid">
        <div class="span4">
            <label for="categoria">Categoria</label>
            <?php echo form_dropdown('categoria', $categoria, set_value('categoria', $acao == 'editar' ? $programa->programa_categoria : ''), 'class="span11"'); ?>
            <?php echo form_error('categoria'); ?>
        </div>
        <div class="span8">
            <label for="empresa">Empresa</label>
            <?php echo form_input($empresa); ?>
            <?php echo form_error('empresa'); ?>
        </div>
    </div>
    <br>
  <div class="row-fluid">
      <div class="span6">
          <label for="link">Link</label>
              <?php echo form_input(array('name'=>'link', 'id' => 'link', 'value'=>set_value('link', $acao == 'editar' ? $programa->programa_link : ''), 'class' => 'span11',)); ?>
              <?php echo form_error('link'); ?>
      </div>
      <div class="control-group">
            <label class="control-label" for="data_expiracao">Data de expiração</label>
            <div class="controls">
              <?php echo form_input('data_expiracao', set_value('data_expiracao', $acao == 'editar' ? date('d/m/Y', $programa->programa_data_expiracao) : ''), 'id="datepicker"'); ?>
              <span class="help-inline"><?php echo form_error('data_expiracao'); ?></span>
            </div>
     </div>
     <?php if($acao == 'editar'): ?>
     <img src="<?php echo base_url(); ?>assets/img/programas/banners/<?php echo $programa->programa_imagem; ?>" alt="<?php echo $programa->programa_empresa; ?>" >
     <?php endif; ?>
     <div class="control-group">
            <label class="control-label" for="imagem"><?php echo ($acao == 'editar' ? 'Alterar Imagem' : 'Imagem'); ?></label>
            <div class="controls">
              <?php echo form_upload('imagem', set_value('imagem'), 'id="datepicker"'); ?>
              <span class="help-inline"><?php echo form_error('imagem'); ?></span>
            </div>
     </div>
     <div class="control-group">
            <div class="controls">
              <label for="destaqueimagem" style="float: left;">Destaque</label>
              <?php 
                $data = array(
                  'name'        => 'destaque',
                  'id'          => 'destaque',
                  'value'       => '1',
                  'checked'     => ($acao == 'editar' && $programa->programa_destaque == '1') ? TRUE : FALSE,
                  );

              echo form_checkbox($data);
              ?>
              <span class="help-inline"><?php echo form_error('destaque'); ?></span>
            </div>
     </div>
  </div>
  <?php echo form_submit('submit', ($acao == 'editar') ? 'Salvar' : 'Cadastrar', 'class="btn btn-primary"'); ?>
  <?php echo form_close(); ?> 
</div>