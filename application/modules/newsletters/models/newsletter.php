<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* news
*
* Model responsável pelas regras de negócio dos newss de estágio, trainee e aprendiz
*
* @author Nilton de Freitas - Trupe
* @link http://trupe.net
*
*/
Class Newsletter extends Datamapper{
    var $table = 'newsletters';

     public function __construct()
    {
        // model constructor
        parent::__construct();
    }
    /**
     * Retorna os newss ativos em ordem crescente de antiguidade
     * @param    int    $limit   quantidade limite por retorno
     * @param    int    $offset  offset para a paginação
     * @return   array           array com a lista de newss ativos
     */
    function get_all()
    {
        $news = new Newsletter();
        $news->order_by('cadastro', 'desc');
        $news->get();

        $arr = array();
        foreach($news->all as $newss)
        {
            $arr[] = $newss;
        }

        return $arr;

    }

    function get_limit($limit, $offset)
    {
        $news = new Newsletter();
        $news->order_by('cadastro', 'desc');
        $news->get($limit, $offset);

        $arr = array();
        foreach($news->all as $newss)
        {
            $arr[] = $newss;
        }

        return $arr;

    }

    function cadastra($dados){

        $news = new Newsletter();

        $news->nome = $dados['nome'];
        $news->email = $dados['email'];
        $news->cadastro = time();

        if($news->save()){
            return TRUE;
        } else
        {
            return FALSE;
        }

    }
}