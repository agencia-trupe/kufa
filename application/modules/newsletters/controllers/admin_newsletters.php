<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_newsletters extends MX_Controller
{
    function index()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
            $this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Meta Talentos - Painel de Controle';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            $this->load->model('newsletters/newsletter');
            $data['title'] = 'Meta Talentos - Newsletter';
            $data['module'] = 'newsletter';
            $data['main_content'] = 'newsletters_view';

            $page = ($this->uri->segment(3) ? $this->uri->segment(3) : 1);
            $this->load->library('pagination');
            $pagination_config = array(
                        'base_url'       => base_url() . 'painel/newsletter/',
                        'total_rows'     => count($this->newsletter->get_all()),
                        'per_page'       => 10,
                        'num_links'      => 5,
                        'next_link'      => 'próximo',
                        'prev_link'      => 'anterior',
                        'first_link'     => FALSE,
                        'last_link'      => FALSE,
                        'use_page_numbers' => TRUE,
                        'full_tag_open'  => '<div class="pagination center"><ul>',
                        'full_tag_close' => '</ul></div>',
                        'cur_tag_open'   => '<li class="active"><a href="#">',
                        'cur_tag_close'  => '</a></li>',
                        'num_tag_open'   => '<li>',
                        'num_tag_close'  => '</li>',
                        'next_tag_open'   => '<li>',
                        'next_tag_close'  => '</li>',
                        'prev_tag_open'   => '<li>',
                        'prev_tag_close'  => '</li>',
                );
            $this->pagination->initialize($pagination_config);
            $data['registros'] = $this->newsletter->get_limit($pagination_config['per_page'], $pagination_config['per_page']*($page -1));
            $this->load->view('includes/template', $data);
        }
    }

    function seleciona()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
            $this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Meta Talentos - Painel de Controle';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            $config = array(
                array(
                    'field' => 'data-de',
                    'label' => 'de',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'data-para',
                    'label' => 'até',
                    'rules' => 'required',
                ),
            );
            $this->load->library('form_validation');
            $this->form_validation->set_rules($config);
            $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');

            if($this->form_validation->run() == FALSE ){
                 $data['title'] = 'Meta Talentos - Newsletter';
                $data['module'] = 'newsletter';
                $data['main_content'] = 'newsletters_view';
                $this->load->view('includes/template', $data);
            }
            else
            {
                $from = strtotime(str_replace('/', '-', $this->input->post('data-de')));
                $to = strtotime(str_replace('/', '-', $this->input->post('data-para')));

                $query = $this->db->query("SELECT `nome` 
                    ,  `email` 
                    FROM `newsletters` WHERE `cadastro` > $from AND
                     `cadastro` < $to");
                if($query->num_rows() > 0)
                {
                    $this->load->helper('csv');
                    echo query_to_csv($query, TRUE, 'newsletter.csv');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Nenhum registro foi
                        encontrado para a data solicitada');
                                redirect('painel/newsletter');
                }
            }

        }
    }

    function add()
    {
        $this->load->library('form_validation');
        $this->load->library('typography');

        $this->form_validation->set_rules('nome', 'nome',
            'required|max_length[255]');
        $this->form_validation->set_rules('email', 'email',
            'trim|valid_email|required');
        $this->form_validation->set_error_delimiters('', '');

        if($this->form_validation->run() == FALSE)  
        {
                echo validation_errors();
        }
        else
        {
            $this->load->model('newsletters/newsletter');

            $dados = array(
                'nome' => $this->input->post('nome'),
                'email' => $this->input->post('email'),
            );

           if($this->newsletter->cadastra($dados))
           {
                echo 'Cadastro efetuado com sucesso!';
           }
           else
           {
                echo 'Houve um erro ao efetuar seu cadastro,
                por favor, tente novamente.';
           }
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */