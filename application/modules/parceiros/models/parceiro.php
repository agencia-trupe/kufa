<?php
/**
* File: parceiro.php
* 
* PHP version 5.3
*
* @category Kufa
* @package  Kufa
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Parceiro
 * 
 * @category Kufa
 * @package  Models
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/

class Parceiro extends Datamapper
{
	/**
	 * Tabela do banco de dados.
	 * 
	 * @var string
	 */
	var $table = 'parceiros';

	/**
	 * Retorna todos os registros do banco de dados
	 * 
	 * @return array registros
	 */
	function get_all()
	{
		$parceiro = new Parceiro();
		$parceiro->order_by('ordem', 'ASC')->get();
		$result = array();
		foreach($parceiro->all as $parceiro)
		{
			$result[] = $parceiro;
		}
		if(sizeof($result))
		{
			return $result;
		}
		return FALSE;
	}

	/**
	 * Obtem os dados de um registros baseado em um valor e tipo
	 * 
	 * @param mixed  $valor parâmetro para busca
	 * @param string $tipo  tipo de parâmetro.
	 * 
	 * @return object objeto
	 */
	function get_conteudo( $valor, $tipo )
	{
		$parceiro = new Parceiro();
		$parceiro->where( $tipo, $valor )->get();
		if( $parceiro->exists() ){
			return $parceiro;
		}
		return NULL;
	}

	/**
	 * Atualiza as informações de um registro
	 * 
	 * @param array $dados dados para atualização.
	 * 
	 * @return array informações atualizadas
	 */
	function change($dados)
	{
		$parceiro = new Parceiro();
		$parceiro->where('id', $dados['id']);
		$update_data = array();
		foreach ($dados as $chave => $valor)
		{
			$update_data[$chave] = $valor;
		}
		$update_data['updated'] = time();
		$update = $parceiro->update($update_data);
		if($update)
		{
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Insere um novo registro no banco
	 * 
	 * @param array $dados dados do registro.
	 * 
	 * @return boolean status
	 */
	function insert($dados)
	{
		$serviços = new Parceiro();
        $serviços->get();
        $count = $serviços->result_count();

		$parceiro = new Parceiro();
		foreach ($dados as $chave => $valor)
		{
			$parceiro->$chave = $valor;
		}
		$parceiro->created = time();
		$parceiro->ordem = $count;
		$insert = $parceiro->save();
		if($insert)
		{
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Remove um registro do banco de dados
	 * 
	 * @param int $parceiro_id id do serviço.
	 * 
	 * @return boolean status
	 */
	function apaga($parceiro_id)
	{
		$parceiro = new Parceiro();
		$parceiro->where('id', $parceiro_id)->get();
		if($parceiro->delete())
		{
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Ordena os dados para exibição
	 * 
	 * @param array $dados array com dados para ordenar
	 * 
	 * @return boolean status
	 */
	function ordena($dados)
	{
		$result = array();
		foreach($dados as $chave => $valor)
		{
			$parceiro = new Parceiro();
			$parceiro->where('id', $valor);
			$update_data = array(
				'ordem' => $chave
				);
			if($parceiro->update($update_data))
			{
				$result[] = $valor;
			}
		}
		if(sizeof($result))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}

/* End of file parceiro.php */
/* Location: ./modules/parceiros/models/parceiro.php */