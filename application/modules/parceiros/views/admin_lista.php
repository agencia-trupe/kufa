<div class="row-fluid">
    <div class="span9">
         <legend>Parceiros <a class="btn btn-info btn-mini" href="<?=site_url('painel/parceiros/add'); ?>">Novo</a></legend>
         <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Nome</th><th>Cidade/UF</th><th class="span2"><i class="icon-cog"></i></th>
            </tr>
        </thead>
        <tbody>
        <?php if($parceiros): ?>
            <?php foreach ($parceiros as $parceiro): ?>
                <tr>
                    <td><?php echo $parceiro->nome ?></td>
                    <td><?=$parceiro->cidade; ?>/<?php echo $parceiro->uf ?></td>
                    <td>
                        <?=anchor('painel/parceiros/edit/' . $parceiro->id, 'Editar', 'class="btn btn-mini btn-warning"'); ?>
                        <?php echo form_open('painel/parceiros/delete', 'style="display:inline"') ?>
                            <?php echo form_hidden('id', $parceiro->id) ?>
                            <?php echo form_submit('', 'apagar', 'class="btn btn-mini btn-danger"') ?>
                        <?php echo form_close() ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
    </table>
    </div>
</div>