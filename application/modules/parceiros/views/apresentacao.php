<div class="links">
	<div class="parceiros-accordion">
		<?php foreach ($parceiros as $parceiro): ?>
			<h3><?php echo $parceiro->cidade ?>, <?php echo $parceiro->uf ?> - <?php echo $parceiro->nome ?></h3>
			<div class="acordion-content">
				<p>
					<?php echo $parceiro->telefone ?><br>
					<?php echo $parceiro->endereco; ?>,<br>
					nº <?php echo $parceiro->numero ?>, <?php echo $parceiro->complemento ?>, <?php echo $parceiro->bairro ?>,<br>
					CEP: <?php echo $parceiro->cep ?>, <?php echo $parceiro->cidade ?>/<?php echo $parceiro->uf ?>
				</p>
				<div class="clearfix"></div>
			</div>
		<?php endforeach ?>
	</div>
</div>