<a href="#" id="ui-carousel-prev">Anterior</a>
<a href="#" id="ui-carousel-next">Próximo</a>
<div class="parceiros-slider-wrapper">
	<div class="parceiros-slider">
		<?php foreach ($parceiros as $parceiro): ?>
			<a href="<?php echo $parceiro->link ?>" class="parceiro">
				<img src="<?php echo base_url('assets/img/parceiros/' . $parceiro->imagem) ?>" alt="<?php echo $parceiro->nome ?>">
			</a>
		<?php endforeach ?>
	</div>
</div>