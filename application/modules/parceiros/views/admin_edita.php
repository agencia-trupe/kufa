<div class="row-fluid">
    <div class="span9">
        <?php if(isset($error)): ?>
    <div class="alert alert-error">
        <?php echo $error; ?>
    </div>
    <?php endif; ?> 
       <legend><?=($acao == 'editar') ? 'Editar' : 'Cadastrar'; ?> Parceiro</legend>
    <?php 
            switch ($acao) {
                case 'editar':
                    $action = 'painel/parceiros/edit';
                    break;
                
                default:
                    $action = 'painel/parceiros/add';
                    break;
            }
    ?>
    <?=form_open_multipart($action); ?>
    <?=form_hidden( 'id', ( $acao == 'editar') ? $parceiro->id : '' ); ?>
    <?=form_label('Nome'); ?>
    <?=form_input(array(
        'name' => 'nome',
        'value' => set_value('nome', ( $acao == 'editar') ? $parceiro->nome : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('nome'); ?>
    
    <?=form_label('Link'); ?>
    <?=form_input(array(
        'name' => 'link',
        'value' => set_value('link', ( $acao == 'editar') ? $parceiro->link : ''),
    )); ?>
    <?=form_error('link'); ?>

    <?=form_label('Telefone'); ?>
    <?=form_input(array(
        'name' => 'telefone',
        'value' => set_value('telefone', ( $acao == 'editar') ? $parceiro->telefone : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('telefone'); ?>

    <?=form_label('Endereço'); ?>
    <?=form_input(array(
        'name' => 'endereco',
        'value' => set_value('endereco', ( $acao == 'editar') ? $parceiro->endereco : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('endereco'); ?>

    <?=form_label('Número'); ?>
    <?=form_input(array(
        'name' => 'numero',
        'value' => set_value('numero', ( $acao == 'editar') ? $parceiro->numero : ''),
        'class' => 'span1'
    )); ?>
    <?=form_error('numero'); ?>

    <?=form_label('Complemento'); ?>
    <?=form_input(array(
        'name' => 'complemento',
        'value' => set_value('complemento', ( $acao == 'editar') ? $parceiro->complemento : ''),
        'class' => 'span4'
    )); ?>
    <?=form_error('complemento'); ?>

    <?=form_label('Bairro'); ?>
    <?=form_input(array(
        'name' => 'bairro',
        'value' => set_value('bairro', ( $acao == 'editar') ? $parceiro->bairro : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('bairro'); ?>

    <?=form_label('Cidade'); ?>
    <?=form_input(array(
        'name' => 'cidade',
        'value' => set_value('cidade', ( $acao == 'editar') ? $parceiro->cidade : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('cidade'); ?>

    <?=form_label('UF'); ?>
    <?=form_input(array(
        'name' => 'uf',
        'value' => set_value('uf', ( $acao == 'editar') ? $parceiro->uf : ''),
        'class' => 'span1'
    )); ?>
    <?=form_error('uf'); ?>

    <?=form_label('CEP'); ?>
    <?=form_input(array(
        'name' => 'cep',
        'value' => set_value('cep', ( $acao == 'editar') ? $parceiro->cep : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('cep'); ?>
    <br>
    <?php if(isset($parceiro->imagem)):?>
    <img width="200px" src="<?php echo base_url(); ?>assets/img/parceiros/<?php echo $parceiro->imagem; ?>" alt="" >
    <?php endif; ?>
    <div class="control-group">
        <label class="control-label" for="imagem">Alterar Imagem</label>
        <div class="controls">
            <?php echo form_upload('imagem', set_value('imagem')); ?>
            <span class="help-inline"><?php echo form_error('imagem'); ?></span>
        </div>
    </div>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('painel/parceiros', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?> 
    </div>
</div>