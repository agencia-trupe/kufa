<?php
/**
* File: parceiros.php
* 
* PHP version 5.3
*
* @category Kufa
* @package  Kufa
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Parceiros
 * 
 * @category Kufa
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/

class Parceiros extends MX_Controller
{
	/**
	 * Array de variáveis para a view.
	 * 
	 * @var array
	 */
	public $data;

	/**
	 * Construtor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('parceiros/parceiro');
	}

	public function parcial()
	{
		$parceiros = $this->parceiro->get_all();
		$this->data['parceiros'] = $parceiros;
		$this->load->view('parceiros/parcial', $this->data);
	}

	public function apresentacao()
	{
		$parceiros = $this->parceiro->get_all();
		$this->data['parceiros'] = $parceiros;
		$this->load->view('parceiros/apresentacao', $this->data);
	}
}

/* End of file parceiros.php */
/* Location: ./modules/parceiros/controllers/parceiros.php */