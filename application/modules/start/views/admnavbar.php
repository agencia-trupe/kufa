    <div class="navbar navbar-fixed-top navbar-inverse">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?php echo base_url(); ?>">Kufa</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="<?php echo ($module == 'slideshow') ? 'active' : ''; ?>">
                <a href="<?php echo base_url(); ?>painel/slideshow">Slides</a>
              </li>
              <li class="<?php echo ($module == 'apresentacao') ? 'active' : ''; ?>">
                <a href="<?php echo base_url(); ?>painel/apresentacao">Apresentação</a>
              </li>
              <li class="<?php echo ($module == 'equipe') ? 'active' : ''; ?>">
                <a href="<?php echo base_url(); ?>painel/equipe">Equipe</a>
              </li>
              <li class="<?php echo ($module == 'atuacao') ? 'active' : ''; ?>">
                <a href="<?php echo base_url(); ?>painel/atuacao">Áreas de atuação</a>
              </li>
              <li class="<?php echo ($module == 'noticias') ? 'active' : ''; ?>">
                <a href="<?php echo base_url(); ?>painel/noticias">Notícias</a>
              </li>
              <li class="<?php echo ($module == 'eventos') ? 'active' : ''; ?>">
                <a href="<?php echo base_url(); ?>painel/eventos">Eventos</a>
              </li>
              <li class="<?php echo ($module == 'fotos') ? 'active' : ''; ?>">
                <a href="<?php echo base_url(); ?>painel/fotos">Fotos</a>
              </li>
              <li class="<?php echo ($module == 'newsletter') ? 'active' : ''; ?>">
                <a href="<?php echo base_url(); ?>painel/newsletter">Boletim</a>
              </li>
              <li class="<?php echo ($module == 'contato') ? 'active' : ''; ?>">
                <a href="<?php echo base_url(); ?>painel/contato">Contato</a>
            </ul>
            <ul class="nav pull-right">
                <li><?php echo anchor('logout', 'Sair'); ?></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
