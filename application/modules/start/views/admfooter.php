
            
          </div>

</div><!--/row-->      
<hr>

      <footer>
      </footer>

    </div><!--/.fluid-container-->
<!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.8.2.min.js"><\/script>')</script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/libs/jquery.ui.datepicker-pt-BR.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/admin.js"></script>
 <script>
        $(function(){
        // bind change event to select
        $('#qtd-inscricao').bind('change', function () {
            var url = $(this).val(); // get selected value
            if (url) { // require a URL
                window.location = url; // redirect
            }
            return false;
        });
        });
    </script>

  <script type="text/javascript">
    $(function(){
    $("#datepicker").datepicker({dateFormat: 'dd/mm/yy'});
    });
  </script>
 <script>
	$(function() {
    var dates = $( "#from, #to" ).datepicker({
        defaultDate: "+1w",
        numberOfMonths: 1,
        beforeShow: function( ) {
            var other = this.id == "from" ? "#to" : "#from";
            var option = this.id == "from" ? "maxDate" : "minDate";
            var selectedDate = $(other).datepicker('getDate');
            
            $(this).datepicker( "option", option, selectedDate );
        }
    }).change(function(){
        var other = this.id == "from" ? "#to" : "#from";
        
        if ( $('#from').datepicker('getDate') > $('#to').datepicker('getDate') )
            $(other).datepicker('setDate', $(this).datepicker('getDate') );
    });
});
	</script>


  <!-- scripts concatenated and minified via ant build script-->
  <script defer src="<?php echo base_url(); ?>assets/js/libs/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/libs/tiny_mce/jquery.tinymce.js"></script>
     
    <script type="text/javascript">
    $(function(){$("textarea.tinymce").tinymce({
    // Location of TinyMCE script
    script_url : "<?php echo base_url(); ?>assets/js/libs/tiny_mce/tiny_mce.js",
     
    // General options
    theme : "advanced",
    plugins : "safari,pagebreak,maxchars,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",
     
    // Theme options
    theme_advanced_buttons1 : "styleselect,formatselect,fontselect,fontsizeselect",
    theme_advanced_buttons2 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link",
    theme_advanced_buttons3 : "bullist,numlist",
    theme_advanced_buttons4 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    
    max_chars_indicator : "characterCounter",  
    // Drop lists for link/image/media/template dialogs
    template_external_list_url : "lists/template_list.js",
    external_link_list_url : "lists/link_list.js",
    external_image_list_url : "lists/image_list.js",
    media_external_list_url : "lists/media_list.js"
    });});
    </script>
<script>
$(function(){$("textarea.tinymce1").tinymce({
    // Location of TinyMCE script
    script_url : "<?php echo base_url(); ?>assets/js/libs/tiny_mce/tiny_mce.js",
     
    // General options
    theme : "advanced",
    plugins : "safari,pagebreak,maxchars,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",
     
    // Theme options
    theme_advanced_buttons1 : "styleselect,formatselect,fontselect,fontsizeselect",
    theme_advanced_buttons2 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link",
    theme_advanced_buttons3 : "bullist,numlist",
    theme_advanced_buttons4 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    
    max_chars_indicator : "characterCounter1",  
    // Drop lists for link/image/media/template dialogs
    template_external_list_url : "lists/template_list.js",
    external_link_list_url : "lists/link_list.js",
    external_image_list_url : "lists/image_list.js",
    media_external_list_url : "lists/media_list.js"
    });});
</script>
<script>
    $(function(){$("textarea.tinymce2").tinymce({
    // Location of TinyMCE script
    script_url : "<?php echo base_url(); ?>assets/js/libs/tiny_mce/tiny_mce.js",
     
    // General options
    theme : "advanced",
    plugins : "safari,pagebreak,maxchars,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",
     
    // Theme options
    theme_advanced_buttons1 : "styleselect,formatselect,fontselect,fontsizeselect",
    theme_advanced_buttons2 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link",
    theme_advanced_buttons3 : "bullist,numlist",
    theme_advanced_buttons4 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    
    max_chars_indicator : "characterCounter2",  
    // Drop lists for link/image/media/template dialogs
    template_external_list_url : "lists/template_list.js",
    external_link_list_url : "lists/link_list.js",
    external_image_list_url : "lists/image_list.js",
    media_external_list_url : "lists/media_list.js"
    });});
</script>
<script>
$(function(){$("textarea.tinymce3").tinymce({
    // Location of TinyMCE script
    script_url : "<?php echo base_url(); ?>assets/js/libs/tiny_mce/tiny_mce.js",
     
    // General options
    theme : "advanced",
    plugins : "safari,pagebreak,maxchars,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",
     
    // Theme options
    theme_advanced_buttons1 : "styleselect,formatselect,fontselect,fontsizeselect",
    theme_advanced_buttons2 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link",
    theme_advanced_buttons3 : "bullist,numlist",
    theme_advanced_buttons4 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    
    max_chars_indicator : "characterCounter3",  
    // Drop lists for link/image/media/template dialogs
    template_external_list_url : "lists/template_list.js",
    external_link_list_url : "lists/link_list.js",
    external_image_list_url : "lists/image_list.js",
    media_external_list_url : "lists/media_list.js"
    });
});
</script>
<script>
$(function(){$("textarea.tinymce4").tinymce({
    // Location of TinyMCE script
    script_url : "<?php echo base_url(); ?>assets/js/libs/tiny_mce/tiny_mce.js",
     
    // General options
    theme : "advanced",
    plugins : "safari,pagebreak,maxchars,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",
     
    // Theme options
    theme_advanced_buttons1 : "styleselect,formatselect,fontselect,fontsizeselect",
    theme_advanced_buttons2 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link",
    theme_advanced_buttons3 : "bullist,numlist",
    theme_advanced_buttons4 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    
    max_chars_indicator : "characterCounter4",  
    // Drop lists for link/image/media/template dialogs
    template_external_list_url : "lists/template_list.js",
    external_link_list_url : "lists/link_list.js",
    external_image_list_url : "lists/image_list.js",
    media_external_list_url : "lists/media_list.js"
    });});
</script>

<script>
$(function(){$("textarea.tinymce5").tinymce({
    // Location of TinyMCE script
    script_url : "<?php echo base_url(); ?>assets/js/libs/tiny_mce/tiny_mce.js",
     
    // General options
    theme : "advanced",
    plugins : "safari,pagebreak,maxchars,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",
     
    // Theme options
    theme_advanced_buttons1 : "styleselect,formatselect,fontselect,fontsizeselect",
    theme_advanced_buttons2 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link",
    theme_advanced_buttons3 : "bullist,numlist",
    theme_advanced_buttons4 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    
    max_chars_indicator : "characterCounter5",  
    // Drop lists for link/image/media/template dialogs
    template_external_list_url : "lists/template_list.js",
    external_link_list_url : "lists/link_list.js",
    external_image_list_url : "lists/image_list.js",
    media_external_list_url : "lists/media_list.js"
    });});
</script>

<script>

$('#removelink').click(function(e) {  
  
    e.preventDefault();  
    thisHref    = $(this).attr('href');  
  
    if(confirm('Você tem certeza que deseja remover esse registro?')) {  
        window.location = thisHref;  
    }  
  
});  
</script>
  <!-- end scripts-->
  <script>
     
    $("#campo").change(function(){
 
        if ($(this).val() == "data" ) {
 
            
            $("#valor").hide(); //Slide Down Effect
            $("#minrange").show();
            $("#maxrange").show();
 
        } else {
 
            $("#valor").show(); 
            $("#minrange").hide();//Slide Up Effect
            $("#maxrange").hide();
 
        }
    });
  </script>
	
  <!-- Change UA-XXXXX-X to be your site's ID -->
  <script>
    window._gaq = [['_setAccount','UAXXXXXXXX1'],['_trackPageview'],['_trackPageLoadTime']];
    Modernizr.load({
      load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
    });
  </script>
  <script>
      $(document).ready(function(){
  $(".alert").alert({selector : '.alert_close'});
});
    </script>

  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7 ]>
    <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
    <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
  <![endif]-->
  
</body>
</html>