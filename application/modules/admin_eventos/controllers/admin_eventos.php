<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_eventos extends MX_Controller
{
    function index(){

        $this->lista_proximos();
    }
    /**
    *Lista todos os eventos cadastrados atualmente
    *
    * @return [type] [description]
    */
    function lista_proximos()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Kufa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
          {
            $this->load->library('pagination');
            $this->load->library('table');
            $pagination_config = array(
                        'base_url'       => site_url() . 'admin_eventos/lista_proximos/',
                        'total_rows'     => $this->db->where('data >', time())->get('eventos')->num_rows(),
                        'per_page'       => 30,
                        'num_links'      => 5,
                        'next_link'      => 'próximo',
                        'prev_link'      => 'anterior',
                        'first_link'     => FALSE,
                        'last_link'      => FALSE, 
                        'full_tag_open'  => '<div class="pagination center"><ul>',
                        'full_tag_close' => '</ul></div>',
                        'cur_tag_open'   => '<li class="active"><a href="#">',
                        'cur_tag_close'  => '</a></li>',
                        'num_tag_open'   => '<li>',
                        'num_tag_close'  => '</li>',
                        'next_tag_open'   => '<li>',
                        'next_tag_close'  => '</li>',
                        'prev_tag_open'   => '<li>',
                        'prev_tag_close'  => '</li>',
                );
                $this->pagination->initialize($pagination_config);
                //Obtendo resultados no banco
                $this->load->model('eventos/evento');
                $data['result'] = $this->evento->
                get_all_all($pagination_config['per_page'], $this->uri->
                    segment(3));

                $data['title'] = 'Kufa - eventos - Lista';
                $data['module'] = 'eventos';
                $data['main_content'] = 'e_lista_view';
                $this->load->view('includes/template', $data);
          }
          else
          {
          Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de 
            acesso area administrativa sem privilégios');
          $this->session->set_flashdata('error', 'Erro de permissão. 
                                                Você precisa ser 
                                                administrador para realizar essa ação');
          redirect();

          } 
        }
    }

    function lista_realizados()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Kufa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
          {
            $this->load->library('pagination');
            $this->load->library('table');
            $pagination_config = array(
                        'base_url'       => site_url() . 'admin_eventos/lista_realizados/',
                        'total_rows'     => $this->db->where('data <', time())->get('eventos')->num_rows(),
                        'per_page'       => 30,
                        'num_links'      => 5,
                        'next_link'      => 'próximo',
                        'prev_link'      => 'anterior',
                        'first_link'     => FALSE,
                        'last_link'      => FALSE, 
                        'full_tag_open'  => '<div class="pagination center"><ul>',
                        'full_tag_close' => '</ul></div>',
                        'cur_tag_open'   => '<li class="active"><a href="#">',
                        'cur_tag_close'  => '</a></li>',
                        'num_tag_open'   => '<li>',
                        'num_tag_close'  => '</li>',
                        'next_tag_open'   => '<li>',
                        'next_tag_close'  => '</li>',
                        'prev_tag_open'   => '<li>',
                        'prev_tag_close'  => '</li>',
                );
                $this->pagination->initialize($pagination_config);
                //Obtendo resultados no banco
                $this->load->model('eventos/evento');
                $data['result'] = $this->evento->
                get_all_realizados($pagination_config['per_page'], $this->uri->
                    segment(3));

                $data['title'] = 'Kufa - Eventos';
                $data['module'] = 'eventos';
                $data['main_content'] = 'e_lista_view';
                $this->load->view('includes/template', $data);
          }
          else
          {
          Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de 
            acesso area administrativa sem privilégios');
          $this->session->set_flashdata('error', 'Erro de permissão. 
                                                Você precisa ser 
                                                administrador para realizar essa ação');
          redirect();

          } 
        }
    }
    /**
     * Mostra a página de edição de um evento cujo id foi passado como 
     * parâmetro.
     *
     * @param  [int] $id [description]
     * @return [mixed]     [description]
     */
    function editar($id)
    {
        /**
         * Verifica se o usuário está logado para então prosseguir ou não.
         */
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Kufa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            //Verifica se o usuário tem nível de acesso permitido
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
            {
                $id = $this->uri->segment(4);
                if(!$id)
                {
                    $this->session->set_flashdata('error', 'A ação não pode ser
                    realizada, tente novamente ou entre em contato com o suporte');
                    redirect('painel/eventos/lista');
                }
                else
                {
                    $this->load->model('eventos/evento');
                    $data['module'] = 'eventos';
                    $data['title'] = 'Kufa - Eventos';

                    if($this->evento->get_evento($id))
                    {
                        $data['evento'] = $this->evento->get_evento($id);
                        $data['acao'] = 'editar';
                        $data['main_content'] = 'e_cadastra_view';
                        $this->load->view('includes/template', $data);
                    }
                    else
                    {
                        $this->session->set_flashdata('error', 'A ação não pode ser
                        realizada, tente novamente ou entre em contato com o suporte');
                        redirect('eventos/lista');
                    }
                }
            }
            else
            {
                Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
                redirect();
            }
        }
    }

    function cadastra()
    {

        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Kufa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
              if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
              {
                  $data['title'] = 'Kufa - Eventos';
                  $data['module'] = 'eventos';
                  $data['acao'] = 'cadastra';
                  $data['main_content'] = 'e_cadastra_view';
                  $this->load->view('includes/template', $data);
              }
              else
              {
              Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
              $this->session->set_flashdata('error', 'Erro de permissão.
                                                    Você precisa ser administrador para realizar essa ação');
              redirect();

              }
        }
    }

    function salva(){

        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Kufa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            $config = array(
                array(
                    'field' => 'titulo',
                    'label' => 'titulo',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'conteudo',
                    'label' => 'conteúdo',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'data',
                    'label' => 'data',
                    'rules' => 'required',
                ),
            );
            $this->load->library('form_validation');
            $this->form_validation->set_rules($config);
            $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');
            $data['acao'] = $this->input->post('acao');

            if($this->form_validation->run() == FALSE ){
                  $data['title'] = 'Kufa - Eventos';
                  $data['module'] = 'eventos';
                  $data['main_content'] = 'e_cadastra_view';
                  $this->load->view('includes/template', $data);
            }
            else
            {
                $this->load->model('eventos/evento');

                //prepara o array com os dados para enviar ao model
                $dados = array(
                        'titulo' => $this->input->post('titulo'),
                        'texto' => $this->input->post('conteudo'),
                        'data' => $this->input->post('data'),
                    );

                if( ! $this->evento->cadastra($dados))
                {
                    $this->session->set_flashdata('error', 'A ação não pode ser
                    realizada, tente novamente ou entre em contato com o suporte');
                    redirect('painel/eventos/cadastra');
                }
                else
                {
                    $this->session->set_flashdata('success', 'evento cadastrada
                    com sucesso!');
                    redirect('painel/eventos/lista');
                }
            }
        }
    }

    function atualiza()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Kufa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
            {
                $config = array(
                    array(
                    'field' => 'titulo',
                    'label' => 'titulo',
                    'rules' => 'required',
                    ),
                    array(
                        'field' => 'conteudo',
                        'label' => 'conteúdo',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'data',
                        'label' => 'data',
                        'rules' => 'required',
                    ),
                );
                $this->load->library('form_validation');
                $this->form_validation->set_rules($config);
                $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');

                if($this->form_validation->run() == FALSE )
                {
                    $id  = $this->input->post('id');
                    $this->load->model('eventos/evento');
                    $data['module'] = 'eventos';
                    $data['title'] = 'Kufa - eventos - Editar';
                    $data['evento'] = $this->evento->get_evento($id);
                    $data['acao'] = 'editar';
                    $data['main_content'] = 'n_cadastra_view';
                    $this->load->view('includes/template', $data);
                }
                else
                {

                    $this->load->model('eventos/evento');

                    //prepara o array com os dados para enviar ao model
                    $dados = array(
                                'id' => $this->input->post('id'),
                                'titulo' => $this->input->post('titulo'),
                                'texto' => $this->input->post('conteudo'),
                                'data' => $this->input->post('data'),
                        );


                        if( ! $this->evento->atualiza($dados))
                        {
                            $this->session->set_flashdata('error', 'A ação não pode ser
                            realizada, tente novamente ou entre em contato com o suporte');
                            redirect('painel/eventos/atualiza/' . $this->input->post('id'));
                        }
                        else
                        {
                            $this->session->set_flashdata('success', 'evento atualizado
                            com sucesso!');
                            redirect('painel/eventos/lista');
                        }
                }

              }
              else
              {
              Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
              $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
              redirect();

              }
        }
    }

    function apaga($id)
    {
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Kufa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
            {
                $id = $this->uri->segment(4);
                if(!$id)
                {
                $this->session->set_flashdata('error', 'A ação não pode ser
                    realizada, tente novamente ou entre em contato com o suporte');
                redirect('painel/eventos/lista');
                }
                else
                {
                    $this->load->model('eventos/evento');
                    if($this->evento->delete_evento($id))
                    {
                        $this->session->set_flashdata('success', 'Registro apagado
                        com sucesso');
                         redirect('painel/eventos/lista');
                    }
                     else
                    {
                        $this->session->set_flashdata('error', 'A ação não pode ser
                         realizada, tente novamente ou entre em contado com o suporte');
                        redirect('painel/eventos/lista');
                    }
                }
            }
            else
            {
                Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
                redirect();

            }
        }

    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */