<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* atuacao
*
* @author Nilton de Freitas
* @link http://trupe.net
*
*/
Class Atuacao extends Datamapper{
    var $table = 'atuacoes';

     public function __construct()
    {
        // model constructor
        parent::__construct();
    }

    function get_last()
    {
        $atuacao = new Atuacao();
        $atuacao->order_by('titulo', 'asc');
        $atuacao->limit(1);
        $atuacao->get();

        return $atuacao;
    }

    function get_atuacao($id)
    {
        $atuacao = new Atuacao();
        $atuacao->where('id', $id);
        $atuacao->limit(1);
        $atuacao->get();

        return $atuacao;
    }
    /**
     * Retorna os atuacaos ativos em ordem crescente de antiguidade
     * @param    int    $limit   quantidade limite por retorno
     * @param    int    $offset  offset para a paginação
     * @return   array           array com a lista de atuacaos ativos
     */
    function get_all()
    {
        $t = new atuacao();
        $t->order_by('titulo', 'asc');
        $t->get();

        $arr = array();
        foreach($t->all as $atuacao)
        {
            $arr[] = $atuacao;
        }

        return $arr;

    } 

    function get_home()
    {
        $atuacao = new atuacao();
        $atuacao->order_by('atuacao_data_expiracao', 'asc');
        $atuacao->get(2);

        $arr = array();
        foreach($atuacao->all as $atuacaos)
        {
            $arr[] = $atuacaos;
        }

        return $arr;

    }

    function get_all_all($limit, $offset)
    {
        $atuacao = new atuacao();
        $atuacao->order_by('atuacao_data_expiracao', 'asc');
        $atuacao->order_by('atuacao_data_cadastro', 'desc');
        $atuacao->get($limit, $offset);

        $arr = array();
        foreach($atuacao->all as $atuacaos)
        {
            $arr[] = $atuacaos;
        }

        return $arr;

    }



    function cadastra($dados){

        $atuacao = new atuacao();

        $atuacao->titulo = $dados['titulo'];
        $atuacao->texto = $dados['texto'];

        if($atuacao->save()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }
    function atualiza($dados){

        $atuacao = new atuacao();
        $atuacao->where('id', $dados['id']);
        $update = $atuacao->update(array(
           'titulo' => $dados['titulo'],
           'texto' => $dados['texto'],
        ));
        return $update;
    }
    function get_atuacao_by_id($id)
    {
        $atuacao = new atuacao();
        $atuacao->where('id', $id);
        $atuacao->limit(1);
        $atuacao->get();

        return $atuacao;
    }

     function delete_atuacao($id){
        $atuacao = new atuacao();
        $atuacao->where('id', $id)->get();
        

        if($atuacao->delete()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }
    
}