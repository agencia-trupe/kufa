<?php 

class Atuacoes extends MX_Controller
{
    public function index()
    {
        $this->detalhe();
    }

    public function detalhe($atuacao = NULL)
    {
        $data['slug'] = 'atuacao';
        $data['titulo'] = 'Atuação';
        $this->load->model('atuacoes/atuacao');
        $data['result'] = $this->atuacao->get_all();
        if($atuacao == NULL)
        {
            $data['atuacao'] = $this->atuacao->get_last();
            $data['id'] = $data['atuacao']->id;
            $data['conteudo_principal'] = 'atuacoes/index';
            $this->load->view('layout/template', $data);
        }
        else
        {
            $data['atuacao'] = $this->atuacao->get_atuacao($atuacao);
            $data['id'] = $atuacao;
            $data['conteudo_principal'] = 'atuacoes/index';
            $this->load->view('layout/template', $data);
        }
    }

    public function widget()
    {
        $this->load->model('atuacoes/atuacao');
        $data['result'] = $this->atuacao->get_all();
        $this->load->view('atuacoes/widget', $data);
    }
}