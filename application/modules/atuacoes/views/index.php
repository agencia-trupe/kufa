<h1 class="page">Áreas de Atuação</h1>
<div class="sidebar">
    <div class="links">
        <ul>
            <?php foreach($result as $link): ?>
                <li><a class="<?php echo ($id == $link->id) ? 'active' : ''; ?>" href="<?php echo site_url('atuacao/' . $link->id); ?>"><?php echo $link->titulo; ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<div class="content">
    <div class="text">
        <h1><?php echo $atuacao->titulo; ?></h1>
        <?php echo $atuacao->texto; ?>
    </div>
</div>
<div class="clearfix"></div>