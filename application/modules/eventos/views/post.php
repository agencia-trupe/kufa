<h1 class="page">Notícias e Publicações</h1>
<div class="sidebar">
    <div class="links">
        <ul>
            <li>
                <a class="<?php echo ($post->noticia_categoria == 'noticias') ? 
                    'active' : ''; ?>" href="<?php echo site_url('noticias/'); ?>">
                    Notícias
                </a>
                <li>
                <a class="<?php echo ($post->noticia_categoria == 'obras-artigos') ? 
                    'active' : ''; ?>" href="<?php echo site_url('noticias/obras-artigos/'); ?>">
                    Obras e Artigos
                </a>
            </li>
            <li>
                <a class="<?php echo ($post->noticia_categoria == 'reportagens-concedidas') ? 
                    'active' : ''; ?>" href="<?php echo site_url('noticias/reportagens-concedidas'); ?>">
                    Reportagens concedidas
                </a>
            </li>
            </li>
        </ul>
    </div>
</div>
<div class="content">
    <div class="text post">
        <article>
        <div class="conteudo">
            <div class="data">
                <p>
                    <?php echo date('d', $post->noticia_data_publicacao) . ' de ' 
                    .$this->calendar->get_month_name(date('m', $post->noticia_data_publicacao))
                    .' de ' . date('Y', $post->noticia_data_publicacao); ?>
                </p>
            </div>
            <h1><?php echo $post->noticia_titulo; ?></h1>
            <p class="resumo">
                <?php echo $post->noticia_resumo; ?>
            </p>
            <?php echo $post->noticia_conteudo; ?>
            <a href="<?php echo site_url('noticias/' . $post->noticia_categoria); ?>" class="voltar">Voltar</a>
        </div>
        <div class="clearfix"></div>
        </article>
    </div>
</div>
<div class="clearfix"></div>