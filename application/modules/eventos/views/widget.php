<?php if(isset($eventos)): ?>
    <?php foreach($eventos as $evento): ?>
        <a href="<?php echo site_url('agenda'); ?>" class="evento-bloco">
            <div class="evento-data">
                <p class="data-dia"><?php echo date('d', $evento->data); ?></p>
                <p class="data-mes"><?php echo $this->calendar->get_month_name(date('m', $evento->data)); ?></p>
            </div>
            <div class="evento-titulo">
                <h1><?php echo $evento->titulo; ?></h1>
            </div>
        </a>
    <?php endforeach; ?>
<?php endif; ?>