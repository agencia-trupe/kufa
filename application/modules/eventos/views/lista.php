<h1 class="page">Agenda</h1>
<div class="sidebar">
    <div class="links">
        <ul>
            <li>
                <a class="<?php echo ($tipo == 'proximos') ? 
                    'active' : ''; ?>" href="<?php echo site_url('agenda/'); ?>">
                    Próximos eventos
                </a>
            </li>
            <li>
                <a class="<?php echo ($tipo == 'realizados') ? 
                    'active' : ''; ?>" href="<?php echo site_url('agenda/realizados/'); ?>">
                    Eventos realizados
                </a>
            </li>
        </ul>
    </div>
</div>
<div class="content">
    <div class="text">
        <div id="accordion">
            <?php foreach($results as $result): ?>
                <a class="header" href="#">
                    <h1><?php echo $result->titulo; ?></h1>
                    <p>
                        <?php echo date('d', $result->data) . ' de ' 
                        .$this->calendar->get_month_name(date('m', $result->data))
                        .' de ' . date('Y', $result->data); ?>
                    </p>
                </a>
                <div <?php echo ($result->data < time()) ? 'class="evento-realizado"' : ''; ?> ><?php echo $result->texto; ?>
                  <div class="clearfix"></div>
                </div>
            <?php endforeach; ?>
        </div>
<?php echo $this->pagination->create_links(); ?>
    </div>
</div>
<div class="clearfix"></div>