<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Eventos
*
* @author Nilton de Freitas - Trupe
* @link http://trupe.net
*
*/
Class eventos extends MX_Controller
{
    public function index()
    {
        $this->lista();
    }
    public function home()
    {
        $this->load->model('eventos/evento');
        $data['eventos'] = $this->evento->get_home();
        $this->load->view('eventos/widget', $data);
    }
    public function lista($tipo = NULL)
    {
        $tipo = ($tipo == NULL) ? 'proximos' : $tipo;
        $this->load->library('calendar');
        $this->load->library('pagination');
            $pagination_config = array(
                        'base_url'       => base_url() . 'agenda/' . $tipo . '/',
                        //Verifica o tipo de eventos para calcular o total de 
                        //resultados para a paginação.
                        'total_rows'     => ($tipo == 'proximos') ?
                            //Se o tipo for 'próximos', retorna os resultados cuja
                            //data for > time();
                            $this->db->where('data >', 
                            time())->get('eventos')->num_rows() :
                            //Se o tipo for 'anteriores', retorna os resultados cuja
                            //data for < time();
                            $this->db->where('data <', 
                            time())->get('eventos')->num_rows(),
                        'per_page'       => 3,
                        'num_links'      => 5,
                        'next_link'      => 'próximo',
                        'prev_link'      => 'anterior',
                        'first_link'     => FALSE,
                        'last_link'      => FALSE, 
                        'full_tag_open'  => '<div class="pagination center"><ul>',
                        'full_tag_close' => '</ul></div>',
                        'cur_tag_open'   => '<li class="active"><a href="#">',
                        'cur_tag_close'  => '</a></li>',
                        'num_tag_open'   => '<li>',
                        'num_tag_close'  => '</li>',
                        'next_tag_open'   => '<li>',
                        'next_tag_close'  => '</li>',
                        'prev_tag_open'   => '<li>',
                        'prev_tag_close'  => '</li>',
                );
            $this->pagination->initialize($pagination_config);
            //Obtendo resultados no banco
            $this->load->model('eventos/evento');
            $data['results'] = $this->evento->get_all($tipo, $pagination_config['per_page'], $this->uri->segment(3));

        $data['tipo'] = ($tipo == NULL) ? 'proximos' : $tipo;
        switch ($data['tipo']) 
        {
            case 'proximos':
                $data['titulo'] = 'Próximos eventos';
                break;
            case 'realizados':
                $data['titulo'] = 'Eventos realizados';
                break;
        }
        $data['slug'] = 'agenda';
        $data['conteudo_principal'] = "eventos/lista";
        $this->load->view('layout/template', $data);
    }

    public function post($id)
    {
        $this->load->library('calendar');
        $this->load->model('eventos/evento');
        $data['post'] = $this->evento->get_evento($id);
        $data['relateds'] = $this->evento->get_relate($id);
        $anterior = $this->evento->get_anterior($id);
        $data['anterior'] = $anterior ? $anterior->id : NULL;
        $proximo = $this->evento->get_proximo($id);
        $data['proximo'] = $proximo ? $proximo->id : NULL;
        $data['titulo'] = 'Kufa &middot; Novidades | Artigos';
        $data['slug'] = 'eventos';
        $data['conteudo_principal'] = "eventos/post";
        $this->load->view('layout/template', $data);
    }

    public function widget()
    {
        $this->load->library('calendar');
        $this->load->model('eventos/evento');
        $data['eventos'] = $this->evento->get_widget();
        $this->load->view('eventos/widget', $data);
    }

}