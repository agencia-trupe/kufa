<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Eventos
*
*
* @author Nilton de Freitas - Trupe
* @link http://trupe.net
*
*/
Class Evento extends Datamapper{
    var $table = 'eventos';

     public function __construct()
    {
        // model constructor
        parent::__construct();
    }
    /**
     * Retorna os eventos ativos em ordem crescente de antiguidade
     * @param    int    $limit   quantidade limite por retorno
     * @param    int    $offset  offset para a paginação
     * @return   array           array com a lista de eventos ativos
     */
    function get_all($tipo, $limit, $offset)
    {
        $evento = new Evento();
        ($tipo == 'proximos') ? 
        $evento->where('data >', time()) : $evento->where('data <', time());
        $evento->order_by('data', ($tipo == 'proximos') ? 'asc' : 'desc');
        $evento->get($limit, $offset);

        $arr = array();
        foreach($evento->all as $eventos)
        {
            $arr[] = $eventos;
        }

        return $arr;

    }

    function get_widget()
    {
        $evento = new evento();
        $evento->where('data >', time());
        $evento->order_by('data', 'asc');
        $evento->get(2);

        $arr = array();
        foreach($evento->all as $eventos)
        {
            $arr[] = $eventos;
        }

        return $arr;

    }

    function get_all_all($limit, $offset)
    {
        $evento = new evento();
        $evento->order_by('data', 'asc');
        $evento->where('data >', time());
        $evento->get($limit, $offset);

        $arr = array();
        foreach($evento->all as $eventos)
        {
            $arr[] = $eventos;
        }

        return $arr;

    }

    function get_all_realizados($limit, $offset)
    {
        $evento = new evento();
        $evento->order_by('data', 'desc');
        $evento->where('data <', time());
        $evento->get($limit, $offset);

        $arr = array();
        foreach($evento->all as $eventos)
        {
            $arr[] = $eventos;
        }

        return $arr;

    }



    function cadastra($dados){

        $evento = new evento();

        $evento->titulo = $dados['titulo'];
        $evento->texto = $dados['texto'];
        $evento->data = strtotime(str_replace('/', '-', $dados['data']));;

        if($evento->save()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }

    function atualiza($dados){

        $evento = new evento();
        $evento->where('id', $dados['id']);
        $update = $evento->update(array(
            'titulo' => $dados['titulo'],
            'texto' => $dados['texto'],
            'data' => strtotime(str_replace('/', '-',
                $dados['data'])),
        ));
        return $update;
    }
    function get_evento($id)
    {
        $evento = new evento();
        $evento->where('id', $id);
        $evento->limit(1);
        $evento->get();

        return $evento;
    }

    function get_relate($id)
    {
        $given = new evento();
        $atual = $given->where('id', $id)->get();

        $evento = new evento();
        $evento->order_by('evento_data_publicacao', 'desc');
        $evento->where('evento_data_publicacao <', $atual->evento_data_publicacao);
        $evento->limit(5);
        $evento->get();

        $arr = array();
        foreach($evento->all as $eventos)
        {
            $arr[] = $eventos;
        }

        return $arr;
    }

function get_anterior($id)
    {
        $given = new evento();
        $atual = $given->where('id', $id)->get();

        $anterior = new evento();
        $anterior->order_by('evento_data_publicacao', 'desc');
        $anterior->where('evento_data_publicacao <', $atual->evento_data_publicacao);
        $anterior->limit(1);
        $anterior->get();
        if(!$anterior->exists())
        {
            return FALSE; 
        }
        else
        {
            return $anterior;
        }
    }

    /**
     * Retorna o registro anterior ao registro cujo id foi passado como parâmetro
     * @param  int $id 
     * @return mixed
     */
    function get_proximo($id)
    {
        $given = new evento();
        $atual = $given->where('id', $id)->get();

        $proximo = new evento();
        $proximo->order_by('evento_data_publicacao', 'asc');
        $proximo->where('evento_data_publicacao >', $atual->evento_data_publicacao);
        $proximo->limit(1);
        $proximo->get();
        if(!$proximo->exists())
        {
            return FALSE; 
        }
        else
        {
            return $proximo;
        }
    }


     function delete_evento($id){
        $evento = new evento();
        $evento->where('id', $id)->get();


        if($evento->delete()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }

}