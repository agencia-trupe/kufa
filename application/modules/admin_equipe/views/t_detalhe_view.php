<div class="span9">
    <?php if(isset($item)): ?>
    <div id="curso">
        <header>
            
            <h3>Curso</h3>
            <h1><?php echo $item->titulo; ?></h1>
            <?php if(isset($item->imagem)): ?>
            <img style="margin-bottom:10px;" width="202" height="122" src="<?php echo base_url(); ?>assets/img/itens/<?php echo $item->imagem; ?>" />
            <?php endif; ?>
            <div class="clearfix"></div>
            <?php 
                if(isset($item->imagem))
                {
                    echo anchor("treinamentos/imagem/$item->slug", 'Alterar Imagem', 'class="btn btn-primary"');
                }
                else
                {
                    echo anchor("treinamentos/imagem/$item->slug", 'Adicionar Imagem', 'class="btn btn-primary"');
                }
            
            
             ?> 
            <?php echo anchor("treinamentos/edita/$item->id", 'Editar', 'class="btn btn-primary"'); ?> <?php echo anchor("treinamentos/apaga/$item->id", 'Apagar', 'class="btn btn-danger confirm"'); ?>
                
        </header>
        <div class="clearfix"></div>
        
        <div id="curso-content">
            <dl>
                
                <dt><i class="icon-time"></i> Carga Horária</dt>
                <dd><?php echo $item->horas; ?> horas</dd>
                
                <dt><i class="icon-screenshot"></i> Objetivo</dt>
                <dd> 
                    <?php echo $item->objetivos; ?>
                </dd>
                
                <dt><i class="icon-user"></i> Público Alvo</dt>
                <dd><?php echo $item->publico; ?></dd>
                <dt><i class="icon-ok"></i> Pré-requisitos</dt>
                <dd><?php echo $item->requisitos; ?></dd>
                <dt><i class="icon-list-alt"></i> Programa</dt>
                <dd>
                    <?php echo $item->programa; ?>
                </dd>
                <dt><i class="icon-book"></i> Metodologia</dt>
                <dd>
                    <?php echo $item->metodologia; ?>
                </dd>
            </dl>
        </div>
         
    </div>
    <?php else: ?>
    <p>Treinamento não encontrado</p>
    <?php endif; ?>
    
</div>						
                <div class="clearfix"></div>

		</div>
		
