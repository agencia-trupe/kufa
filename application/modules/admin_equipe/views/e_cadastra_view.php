<div class="span9">

<legend><?php echo $acao == 'editar' ? 'Editar Integrante' : 'Novo Integrante' ;?></legend>
    <?php echo isset($error) ? $error['error'] : ''; ?>
    <?php echo form_open_multipart(($acao == 'editar') ? 'painel/equipe/atualiza' : 'painel/equipe/salva', 'class="well"'); 
    ?>

    <div class="row-fluid">
      <?php echo ($acao == 'edita') ? form_hidden('acao', 'edita') : form_hidden('acao', 'cadastra'); ?>  
    </div>
    <?php if($acao == 'editar') : ?>
      <?php echo form_hidden('id', set_value('id', $integrante->id)); ?>
    <?php endif; ?>
  <div class="row-fluid">
      <div class="span11">
          <label for="nome">Nome</label>
              <?php echo form_input(array('name'=>'nome', 'id' => 'nome', 'value'=>set_value('nome', $acao == 'editar' ? $integrante->nome : ''), 'class' => 'span11',)); ?>
              <?php echo form_error('nome'); ?>
      </div>
      <div class="clearfix"></div>
      <div class="control-group">
            <label class="control-label" for="texto">Texto</label>
            <div class="controls">
              <?php echo form_textarea('texto', set_value('texto', $acao == 'editar' ? $integrante->texto : ''), 'class="tinymce span11"'); ?>
              <span class="help-inline"><?php echo form_error('texto'); ?></span>
            </div>
      </div>
     </div>
     <?php if($acao == 'editar'): ?>
     <?php if($integrante->foto):?>
     <img src="<?php echo base_url(); ?>assets/img/equipe/<?php echo $integrante->foto; ?>" alt="<?php echo $integrante->nome; ?>" >
     <?php endif; ?>
     <?php endif; ?>
     <div class="control-group">
            <label class="control-label" for="imagem"><?php echo ($acao == 'editar' ? 'Alterar Foto' : 'Adicionar Foto'); ?></label>
            <div class="controls">
              <?php echo form_upload('imagem', set_value('imagem')); ?>
              <span class="help-inline"><?php echo form_error('imagem'); ?></span>
            </div>
     </div>
  <?php echo form_submit('submit', ($acao == 'editar') ? 'Salvar' : 'Cadastrar', 'class="btn btn-primary"'); ?>
  <?php echo form_close(); ?> 
</div>