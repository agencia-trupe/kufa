<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_fotos extends MX_Controller
{
    function index(){

            $this->lista();
        }

    function fotos()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
            $this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Kufa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            $data['title'] = 'Kufa &middot; fotos';
            $data['module'] = 'fotos';
            $data['main_content'] = 'fotos_view';
            $this->load->view('includes/template', $data);
        }
    }
    /**
     *Lista todos os fotos cadastrados atualmente
     *
     * @return [type] [description]
     */
    function lista()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Kufa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
          {
            $this->load->library('pagination');
            $tipo = $this->uri->segment(2);
            $pagination_config = array(
                'base_url'       => base_url() . 'admin_fotos/lista/',
                'total_rows'     => $this->db->get('fotos')->num_rows(),
                'per_page'       => 20,
                'num_links'      => 5,
                'next_link'      => 'Próximo <div id="icon-proximo"></div>',
                'prev_link'      => '<div id="icon-anterior"></div> Anterior',
                'first_link'     => FALSE,
                'last_link'      => FALSE, 
                'full_tag_open'  => '<div class="pagination center"><ul>',
                'full_tag_close' => '</ul></div>',
                'cur_tag_open'   => '<li class="active"><a href="#">',
                'cur_tag_close'  => '</a></li>',
                'num_tag_open'   => '<li>',
                'num_tag_close'  => '</li>',
                'next_tag_open'   => '<li>',
                'next_tag_close'  => '</li>',
                'prev_tag_open'   => '<li>',
                'prev_tag_close'  => '</li>',
                );
            $this->pagination->initialize($pagination_config);
            //Obtendo resultados no banco
            $this->load->model('fotos/foto');
            $data['result'] = $this->foto->get_all_admin($pagination_config['per_page'], $this->uri->segment(3));

            //Layout
                $data['title'] = 'Kufa Fotos';
                $data['module'] = 'fotos';
                $data['main_content'] = 'g_lista_view';
                $this->load->view('includes/template', $data);
          }
          else
          {
          Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de 
            acesso area administrativa sem privilégios');
          $this->session->set_flashdata('error', 'Erro de permissão. 
                                                Você precisa ser 
                                                administrador para realizar essa ação');
          redirect();

          }
        }
    }
    /**
     * Mostra a página de edição de um foto cujo id foi passado como 
     * parâmetro.
     *
     * @param  [int] $id [description]
     * @return void
     */
    function edita($id)
    {
        /**
         * Verifica se o usuário está logado para então prosseguir ou não.
         */
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Kufa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            //Verifica se o usuário tem nível de acesso permitido
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
            {
                $id = $this->uri->segment(3);
                if(!$id)
                {
                    $this->session->set_flashdata('error', 'A ação não pode ser
                    realizada, tente novamente ou entre em contato com o suporte');
                    redirect('admin_fotos/lista');
                }
                else
                {
                    $this->load->model('fotos/foto');
                    $data['module'] = 'fotos';
                    $data['title'] = 'Kufa - Fotos - Editar';

                    if($this->foto->get_foto($id))
                    {
                        $data['foto'] = $this->foto->get_foto($id);
                        $data['acao'] = 'editar';
                        $data['main_content'] = 'g_edita_view';
                        $this->load->view('includes/template', $data);
                    }
                    else
                    {
                        $this->session->set_flashdata('error', 'A ação não pode ser
                        realizada, tente novamente ou entre em contato com o suporte');
                        redirect('admin_fotos/lista');
                    }
                }
            }
            else
            {
                Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
                redirect();
            }
        }
    }

    function cadastra()
    {

        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Kufa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
              if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
              {
                  $data['title'] = 'Kufa - Fotos';
                  $data['module'] = 'fotos';
                  $data['acao'] = 'cadastra';
                  $data['main_content'] = 'g_cadastra_view';
                  $this->load->view('includes/template', $data);
              }
              else
              {
              Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
              $this->session->set_flashdata('error', 'Erro de permissão.
                                                    Você precisa ser administrador para realizar essa ação');
              redirect();

              }
        }
    }

    function upload(){

        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Kufa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            $this->load->library('multiupload');

            $config['upload_path']   = './assets/img/fotos'; //if the files does not exist it'll be created
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']   = '8000'; //size in kilobytes
            $config['encrypt_name']  = TRUE;

            $this->multiupload->initialize($config);

            $uploaded = $this->multiupload->up(FALSE);

            if($uploaded['success'])
            {
              //Pass true if you want to create the index.php files
              $success = array();
              foreach($uploaded['success'] as $img)
              {
                                $this->load->library('image_moo');
                                //Is only one file uploaded so it ok to use it with $uploader_response[0].
                                $imagem = $img['full_path'];
                                $big = $img['file_path'] . './display/' . $img['file_name'];
                                $nova_imagem = $img['file_path'] . './thumbs/' . $img['file_name'];

                                if($this->image_moo->load($imagem)->resize_crop(200,140)->save($nova_imagem,true)
                                     && $this->image_moo->load($imagem)->resize(600,600)->save($big,true))
                                {
                                    $success[] = $img;
                                }
              } //prints the result of the operation and analize the data

              if($success > 0)
              {

                $this->load->model('fotos/foto');
                if($this->foto->salva_fotos($success, $this->input->post('titulo'), $this->input->post('descricao')))
                {
                    $this->session->set_flashdata('success', 'Fotos cadastradas com
                    sucesso');
                    redirect('admin_fotos/lista');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Erro ao salvar fotos.
                    Tente novamente ou entre em contato com o suporte.');
                    redirect('admin_fotos/lista');
                }
              }
              else
              {
                    $this->session->set_flashdata('error', 'Erro ao criar miniaturas.
                    Tente novamente ou entre em contato com o suporte.');
                    redirect('admin_fotos/cadastra');
              }
            }
            else
            {
                $this->session->set_flashdata('error', 'Você precisa selecionar
                    um arquivo para upload.');
                    redirect('admin_fotos/cadastra');
            }
        }
    }

    function atualiza()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Kufa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
            {
                $id = $this->input->post('id');
                $tipo = $this->input->post('tipo');
                $titulo = $this->input->post('titulo');
                $descricao = $this->input->post('descricao');

                $this->load->model('fotos/foto');
                if($this->foto->atualiza($id, $titulo, $descricao)){
                    $this->session->set_flashdata('success', 'Legenda alterada
                    com sucesso.');
                    redirect('painel/' . $tipo );
                }
                else
                {
                    $this->session->set_flashdata('error', 'Erro ao alterar
                        legenda. Tente novamente ou entre em contato com o 
                        suporte.');
                    redirect('painel/edita/' . $id );
                }
            }
            else
            {
                Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
                redirect();

            }
        }
    }

    function apaga($id)
    {
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Kufa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
            {
                $id = $this->uri->segment(3);
                if(!$id)
                {
                $this->session->set_flashdata('error', 'A ação não pode ser
                    realizada, tente novamente ou entre em contato com o suporte');
                redirect('admin_fotos/lista');
                }
                else
                {
                    $this->load->model('fotos/foto');
                    if($this->foto->delete_foto($id))
                    {
                        $this->session->set_flashdata('success', 'Registro apagado
                        com sucesso');
                         redirect('admin_fotos/lista');
                    }
                     else
                    {
                        $this->session->set_flashdata('error', 'A ação não pode ser
                         realizada, tente novamente ou entre em contado com o suporte');
                        redirect('painel/' . $tipo);
                    }
                }
            }
            else
            {
                Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
                redirect();

            }
        }

    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */