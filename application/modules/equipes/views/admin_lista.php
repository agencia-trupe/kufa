<div class="row-fluid">
    <div class="span9">
        <legend>
            Equipe  <a class="btn btn-info btn-mini" href="<?=site_url('painel/equipe/add'); ?>">Novo</a>
                    <a href="#" class="ordenar-equipe btn btn-mini btn-info">ordenar equipe</a>
                    <a href="#" class="salvar-ordem-equipe hide btn btn-mini btn-warning">salvar ordem</a>
        </legend>
        <div class="alert alert-info hide equipe-mensagem">
            <span>Para ordenar, arraste o nome até a posição desejada</span>
            <a class="close" data-dismiss="alert" href="#">&times;</a>
        </div>
         <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Nome</th><th class="span2"><i class="icon-cog"></i></th>
            </tr>
        </thead>
        <tbody>
        <?php if($equipes): ?>
            <?php foreach ($equipes as $equipe): ?>
                <tr id="equipe_<?php echo $equipe->id ?>">
                    <td><?php echo $equipe->nome ?></td>
                    <td>
                        <?=anchor('painel/equipe/edit/' . $equipe->id, 'Editar', 'class="btn btn-mini btn-warning"'); ?>
                        <?php echo form_open('painel/equipe/delete', 'style="display:inline"') ?>
                            <?php echo form_hidden('id', $equipe->id) ?>
                            <?php echo form_submit('', 'apagar', 'class="btn btn-mini btn-danger"') ?>
                        <?php echo form_close() ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
    </table>
    </div>
</div>