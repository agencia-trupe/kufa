<div class="row-fluid">
    <div class="span9">
        <?php if(isset($error)): ?>
    <div class="alert alert-error">
        <?php echo $error; ?>
    </div>
    <?php endif; ?> 
       <legend><?=($acao == 'editar') ? 'Editar' : 'Cadastrar'; ?> Equipe</legend>
    <?php 
            switch ($acao) {
                case 'editar':
                    $action = 'painel/equipe/edit';
                    break;
                
                default:
                    $action = 'painel/equipe/add';
                    break;
            }
    ?>
    <?=form_open_multipart($action); ?>
    <?=form_hidden( 'id', ( $acao == 'editar') ? $equipe->id : '' ); ?>
    <?php if ($acao === 'editar'): ?>
        <?php echo form_hidden('slug', $equipe->slug) ?>
    <?php endif ?>
    <?=form_label('Nome'); ?>
    <?=form_input(array(
        'name' => 'nome',
        'value' => set_value('nome', ( $acao == 'editar') ? $equipe->nome : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('nome'); ?>
    
    <?=form_label('Descrição'); ?>
    <?=form_textarea(array(
        'name' => 'texto',
        'value' => set_value('texto', ( $acao == 'editar') ? $equipe->texto : ''),
        'class'=> 'tinymce span9'
    )); ?>
    <?=form_error('texto'); ?>
    <br>
    <?php if(isset($equipe->foto)):?>
    <img width="200px" src="<?php echo base_url(); ?>assets/img/equipe/<?php echo $equipe->foto; ?>" alt="" >
    <?php endif; ?>
    <div class="control-group">
        <label class="control-label" for="foto">Alterar Imagem</label>
        <div class="controls">
            <?php echo form_upload('foto', set_value('foto')); ?>
            <span class="help-inline"><?php echo form_error('foto'); ?></span>
        </div>
    </div>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('painel/equipe', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?> 
    </div>
</div>