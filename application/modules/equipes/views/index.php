<h1 class="page">Equipe</h1>
<?php foreach ($integrantes as $integrante): ?>
    <div class="integrante-wrapper <?php echo ($integrante->slug === 'karina') ? 'principal' : ''; ?>">
        <div class="integrante-imagem">
            <img src="<?php echo base_url('assets/img/equipe/' . $integrante->foto) ?>" alt="<?php echo $integrante->nome ?>">
        </div>
        <div class="integrante-conteudo">
            <h1 class="integrante-nome"><?php echo $integrante->nome ?></h1>
            <div class="integrante-descricao">
                <?php echo $integrante->texto ?>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
<?php endforeach ?>
<div class="clearfix"></div>