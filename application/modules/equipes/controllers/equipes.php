<?php
/**
* File: equipes.php
* 
* PHP version 5.3
*
* @category Kufa
* @package  Kufa
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Equipes
 * 
 * @category Kufa
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Equipes extends MX_Controller
{
    /**
     * Array de variáveis para a view.
     * 
     * @var [type]
     */
    public $data;

    /**
     * Construtor
     */
    public function __construct()
    {
        $this->load->model('equipes/equipe');
        $this->data['slug'] = 'equipe';
        $this->data['pagina'] = 'equipe';
        $this->data['titulo'] = 'Equipe';
    }

    public function index()
    {
        $this->data['integrantes'] = $this->equipe->get_all();
        $this->data['conteudo_principal'] = 'equipes/index';
        $this->load->view('layout/template',     $this->data);
    }
}

/* End of file equipes.php */
/* Location: ./modules/equipes/controllers/equipes.php */