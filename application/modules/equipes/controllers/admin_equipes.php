<?php
/**
* File: admin_equipe.php
*
* PHP version 5.3
*
* @category Kufa
* @package  Kufa
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net
*/

/**
 * Class Admin_equipes
 *
 * @category Kufa
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/
 **/

class Admin_equipes extends AuthController
{
	/**
	 * Array de variáveis a ser passado para a view.
	 *
	 * @var array
	 */
	var $data;

	/**
	 * Construtor
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->model('equipes/equipe');
		$this->data['module'] = 'equipe';
	}

	/**
	 * Ação principal
	 *
	 * @return [type] [description]
	 */
	function index()
	{

		$this->lista();
	}

	/**
	*Lista todos os equipes cadastrados atualmente
	*
	* @return [type] [description]
	*/
	function lista()
	{
		$this->load->library('pagination');
		$this->load->library('table');
		$pagination_config = array(
					'base_url'       => site_url() . 'painel/equipe/lista/',
					'total_rows'     => $this->db->get('integrantes')->num_rows(),
					'per_page'       => 30,
					'num_links'      => 5,
					'next_link'      => 'próximo',
					'prev_link'      => 'anterior',
					'first_link'     => FALSE,
					'last_link'      => FALSE,
					'full_tag_open'  => '<div class="pagination center"><ul>',
					'full_tag_close' => '</ul></div>',
					'cur_tag_open'   => '<li class="active"><a href="#">',
					'cur_tag_close'  => '</a></li>',
					'num_tag_open'   => '<li>',
					'num_tag_close'  => '</li>',
					'next_tag_open'   => '<li>',
					'next_tag_close'  => '</li>',
					'prev_tag_open'   => '<li>',
					'prev_tag_close'  => '</li>',
			);
		$this->pagination->initialize($pagination_config);

		//Obtendo resultados no banco
		$this->data['equipes'] = $this->equipe->
		get_all($pagination_config['per_page'], $this->uri->
			segment(3));

		$this->data['title'] = 'Noticias';
		$this->data['module'] = 'equipe';
		$this->data['conteudo'] = 'equipes/admin_lista';
		$this->load->view('start/template', $this->data);
	}

	/**
	 * Adiciona uma nova notícia
	 *
	 * @return void formulário de cadastro
	 */
	function add()
	{
		$post = array();
		//Define o array com os dados do $_POST;
		foreach ($_POST as $chave => $valor)
		{
			$post[$chave] = $valor;
		}

		//Define a ação como cadastro para passar ao formulário
		$this->data['acao'] = 'cadastra';

		//Caso não haja post, retorna o forumlário de cadastro
		if( ! count( $post ) )
		{
			$this->data['module'] = 'equipe';
			$this->data['conteudo'] = 'admin_edita';
			$this->load->view('start/template', $this->data);
		}
		else
		{
			//Retorna o formulário de cadastro com os erros caso não passe na
			//validação do formulário
			if( ! $this->form_validation->run('equipe') )
				return $this->_form_error_return($this->data['acao']);

			//Checa se houve envio de imagem
			if(strlen($_FILES['foto']['name'])>0)
			{
				//Tenta persistir a imagem e retorna uma exception caso não persista
				try
				{
					$upload = $this->_upload_image();
					$post['foto'] = $upload['file_name'];
				}
				catch (Exception $e)
				{
					return $this->_form_error_return($this->data['acao'], $e->getMessage(), $equipe->id, 'id');
				}
			}

			//Tenta inserir a notícia no banco de dados e retorna uma exception caso haja erro;
			try
			{
				$post['created'] = $_SERVER['REQUEST_TIME'];
				$post['user_id'] = $this->tank_auth->get_user_id();
				$this->equipe->insert($post);
				redirect('painel/equipe');
			}
			catch (Exception $e)
			{
				if($upload)
					unlink($upload['full_path']);
				return $this->_form_error_return($this->data['acao'], $e->getMessage());
			}
		}
	}

	/**
	 * Edita uma notícia.
	 *
	 * @param [int] $equipe_id [description]
	 *
	 * @return [mixed]     [description]
	 */
	function edit($equipe_id = NULL)
	{
		$post = array();
		//Define o array com os dados do $_POST;
		foreach ($_POST as $chave => $valor)
		{
			$post[$chave] = $valor;
		}

		//Define a ação como cadastro para passar ao formulário
		$this->data['acao'] = 'editar';

		//Caso não haja post, retorna o forumlário de cadastro
		if( ! $post )
		{
			try
			{
				$this->data['equipe'] = $this->equipe->get_conteudo($equipe_id, 'id');
			}
			catch (Exception $e)
			{
				//Retorna a página de listagem caso nenhuma página corresponda ao id fornecido
				$this->session->set_flashdata('error', $e->getMessage());
				redirect('painel/equipe');
			}
			$this->data['acao'] = 'editar';
			$this->data['conteudo'] = 'admin_edita';
			$this->load->view('start/template', $this->data);
		}
		else
		{
			$equipe = $this->equipe->get_conteudo($post['id'], 'id');

			//Define a imagem atual, que será apagada em caso de novo upload de imagem
			$old_image = $equipe->foto;

			//Retorna o formulário de cadastro com os erros caso não passe na
			//validação do formulário
			if( ! $this->form_validation->run('equipe') )
				return $this->_form_error_return($this->data['acao'], NULL, $post['id'], 'id');

			//Checa se houve envio de imagem
			if(strlen($_FILES['foto']['name'])>0)
			{
				//Tenta persistir a imagem e retorna uma exception caso não persista
				try
				{
					$upload = $this->_upload_image();
					$post['foto'] = $upload['file_name'];
				}
				catch (Exception $e)
				{
					return $this->_form_error_return($this->data['acao'], $e->getMessage(), $equipe->id, 'id');
				}
			}

			//Tenta inserir a notícia no banco de dados e retorna uma exception caso haja erro;
			try
			{
				$post['updated'] = $_SERVER['REQUEST_TIME'];
				$post['user_id'] = $this->tank_auth->get_user_id();
				$this->equipe->change($post);
				if($upload)
					unlink($upload['file_path'] . $old_image);
				redirect('painel/equipe');
			}
			catch (Exception $e)
			{
				if($upload)
					unlink($upload['full_path']);
				return $this->_form_error_return($this->data['acao'], $e->getMessage(), $post['id'], 'id');
			}
		}
	}

	/**
	 * Deleta uma notícia
	 *
	 * @return void redirect
	 */
	function delete()
	{
		$post = $this->input->post();

		try
		{
			$equipe = $this->equipe->get_conteudo($post['id'], 'id');
			$imagem = $equipe->imagem;
			$this->equipe->apaga($post['id']);
		}
		catch (Exception $e)
		{
			$this->session->set_flashdata( 'error', $e->getMessage() );
			redirect('painel/equipe');
		}
		unlink('./assets/img/equipes/' . $imagem);
		$this->session->set_flashdata( 'success', 'Registro removido' );
		redirect('painel/equipe');

	}

	/**
	 * Retorna o formulário contendo a mensagem de erro
	 *
	 * @param string $acao  	 [cadastro|edição]
	 * @param string $error 	 mensagem de erro
	 * @param mixed  $value 	 valor a buscar no banco de dados
	 * @param string $identifier identificador do valor passado
	 *
	 * @return void formulário
	 */
	private function _form_error_return($acao, $error = NULL, $value = NULL, $identifier = NULL)
	{
		if( $acao === 'editar' )
			$this->data['equipe'] = $this->equipe->get_conteudo($value, $identifier);

		if( $error )
			$this->data['error'] = $error;

		$this->data['module'] = 'equipe';
		$this->data['acao'] = $acao;
		$this->data['conteudo'] = 'admin_edita';
		$this->load->view('start/template', $this->data);
	}

	/**
	 * Realiza o upload de uma imagem
	 *
	 * @throws Exception If cant do upload
	 * @return void dados de upload
	 */
	private function _upload_image()
	{
		$config['upload_path'] = './assets/img/equipe/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '8000';
		$config['max_width']  = '3000';
		$config['max_height']  = '3000';
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('foto') )
			throw new Exception($this->upload->display_errors(), 1);

		$this->load->library('image_moo');
        //Is only one file uploaded so it ok to use it with $uploader_response[0].
        $upload_data = $this->upload->data();
        $file_uploaded = $upload_data['full_path'];
        //Locais dos arquivos alterados
        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];

        if( $this->input->post('slug') === 'karina')
        {
        	if ( ! $this->image_moo->load($file_uploaded)
                                ->resize_crop(216, 404)
                                ->save($new_file,true) )
			throw new Exception("Erro ao redimensionar imagem", 1);
        }
        else
        {
        	if ( ! $this->image_moo->load($file_uploaded)
                                ->resize_crop(216, 170)
                                ->save($new_file,true) )
			throw new Exception("Erro ao redimensionar imagem", 1);
        }

		return $this->upload->data();

	}

	/**
	 * Reordena os serviços para exibição
	 *
	 * @return string status de ordenamento
	 */
	public function sort_equipe()
	{
		$itens = $this->input->post('equipe');
		if ($itens)
		{
			$ordenar = $this->equipe->ordena($itens);
			if($ordenar)
			{
				echo 'Ordenado';
			}
			else
			{
				echo 'Erro!';
			}
		}
		else
		{
			echo 'Erro!';
		}
	}
}

/* End of file admin_equipe.php */
/* Location: ./modules/equipes/controllers/admin_equipes.php */