<?php
/**
* File: equipe.php
* 
* PHP version 5.3
*
* @category Kufa
* @package  Kufa
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Equipe
 * 
 * @category Kufa
 * @package  Kufa
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Equipe extends Datamapper
{
    /**
     * Tabela do banco de dados.
     * 
     * @var string
     */
    var $table = 'integrantes';

    /**
     * Retorna todos os registros do banco de dados
     * 
     * @return array registros
     */
    function get_all()
    {
        $integrante = new Equipe();
        $integrante->order_by('ordem', 'ASC')->get();
        $result = array();
        foreach($integrante->all as $integrante)
        {
            $result[] = $integrante;
        }
        if(sizeof($result))
        {
            return $result;
        }
        return FALSE;
    }

    /**
     * Obtem os dados de um registros baseado em um valor e tipo
     * 
     * @param mixed  $valor parâmetro para busca
     * @param string $tipo  tipo de parâmetro.
     * 
     * @return object objeto
     */
    function get_conteudo( $valor, $tipo )
    {
        $integrante = new Equipe();
        $integrante->where( $tipo, $valor )->get();
        if( $integrante->exists() ){
            return $integrante;
        }
        return NULL;
    }

    /**
     * Atualiza as informações de um registro
     * 
     * @param array $dados dados para atualização.
     * 
     * @return array informações atualizadas
     */
    function change($dados)
    {
        $integrante = new Equipe();
        $integrante->where('id', $dados['id']);
        $update_data = array();
        foreach ($dados as $chave => $valor)
        {
            $update_data[$chave] = $valor;
        }
        $update_data['updated'] = time();
        $update = $integrante->update($update_data);
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Insere um novo registro no banco
     * 
     * @param array $dados dados do registro.
     * 
     * @return boolean status
     */
    function insert($dados)
    {
        $serviços = new Equipe();
        $serviços->get();
        $count = $serviços->result_count();

        $integrante = new Equipe();
        foreach ($dados as $chave => $valor)
        {
            $integrante->$chave = $valor;
        }
        $integrante->created = time();
        $integrante->ordem = $count;
        $insert = $integrante->save();
        if($insert)
        {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Remove um registro do banco de dados
     * 
     * @param int $integrante_id id do serviço.
     * 
     * @return boolean status
     */
    function apaga($integrante_id)
    {
        $integrante = new Equipe();
        $integrante->where('id', $integrante_id)->get();
        if($integrante->delete())
        {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Ordena os dados para exibição
     * 
     * @param array $dados array com dados para ordenar
     * 
     * @return boolean status
     */
    function ordena($dados)
    {
        $result = array();
        foreach($dados as $chave => $valor)
        {
            $integrante = new Equipe();
            $integrante->where('id', $valor);
            $update_data = array(
                'ordem' => $chave
                );
            if($integrante->update($update_data))
            {
                $result[] = $valor;
            }
        }
        if(sizeof($result))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

   
}