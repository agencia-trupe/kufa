<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "static_pages";

$route['equipe/(:any)'] = 'equipes/detalhe/$1';
$route['equipe'] = 'equipes';
$route['atuacao/(:any)'] = 'atuacoes/detalhe/$1';
$route['atuacao'] = 'atuacoes';

$route['noticias'] = 'noticias/noticias';
$route['noticias/noticias/(:num)'] = 'noticias/lista/noticias/$1';
$route['noticias/obras-artigos'] = 'noticias/lista/obras-artigos';
$route['noticias/obras-artigos/(:num)'] = 'noticias/lista/obras-artigos/$1';
$route['noticias/informativos-eleitorais'] = 'noticias/lista/informativos-eleitorais';
$route['noticias/informativos-municipais'] = 'noticias/lista/informativos-municipais';
$route['noticias/informativos-tributarios'] = 'noticias/lista/informativos-tributarios';
$route['noticias/manuais-candidato'] = 'noticias/lista/manuais-candidato';
$route['noticias/manuais-prefeito'] = 'noticias/lista/manuais-prefeito';
$route['noticias/informativos-eleitorais/(:num)'] = 'noticias/lista/informativos-eleitorais/$1';
$route['noticias/informativos-municipais/(:num)'] = 'noticias/lista/informativos-municipais/$1';
$route['noticias/informativos-tributarios/(:num)'] = 'noticias/lista/informativos-tributarios/$1';
$route['noticias/manuais-candidato/(:num)'] = 'noticias/lista/manuais-candidato/$1';
$route['noticias/manuais-prefeito/(:num)'] = 'noticias/lista/manuais-prefeito/$1';

// $route['midia'] = 'midia/lista';
// $route['midia/(:num)'] = 'midia/lista/$1';




$route['agenda'] = 'eventos';
$route['agenda/proximos'] = 'eventos/lista/proximos';
$route['agenda/proximos/(:any)'] = 'eventos/lista/proximos/$1';
$route['agenda/realizados'] = 'eventos/lista/realizados';
$route['agenda/realizados/(:any)'] = 'eventos/lista/realizados/$1';

$route['painel/apresentacao'] = 'admin_apresentacao';
$route['painel/apresentacao/(:any)'] = 'admin_apresentacao/$1';

$route['painel/equipe'] = 'equipes/admin_equipes';
$route['painel/equipe/(:any)'] = 'equipes/admin_equipes/$1';

$route['painel/atuacao'] = 'admin_atuacao';
$route['painel/atuacao/(:any)'] = 'admin_atuacao/$1';

$route['painel/noticias'] = 'admin_noticias';
$route['painel/noticias/(:any)'] = 'admin_noticias/$1';

$route['painel/eventos'] = 'admin_eventos';
$route['painel/eventos/lista'] = 'admin_eventos/lista_proximos';
$route['painel/eventos/(:any)'] = 'admin_eventos/$1';

$route['painel/fotos'] = 'admin_fotos';
$route['painel/fotos/(:any)'] = 'admin_fotos/$1';

$route['painel/slideshow'] = 'admin_slideshow';
$route['painel/slideshow/(:any)'] = 'admin_slideshow/$1';

$route['painel/parceiros'] = 'parceiros/admin_parceiros';
$route['painel/parceiros/(:any)'] = 'parceiros/admin_parceiros/$1';

// $route['painel/midia'] = 'admin_noticias/lista/midia';
// $route['painel/midia/add'] = 'admin_noticias/add/midia';
// $route['painel/midia/edit'] = 'admin_noticias/edit';
// $route['painel/midia/edit/(:num)'] = 'admin_noticias/edit/$1/midia';

$route['painel/imprensa'] = 'imprensa/admin_imprensa';
$route['painel/imprensa/(:any)'] = 'imprensa/admin_imprensa/$1';

$route['painel/contato'] = 'contato/admin_contatos';
$route['painel/contato/(:any)'] = 'contato/admin_contatos/$1';

$route['painel/filiais'] = 'contato/admin_filiais';
$route['painel/filiais/(:any)'] = 'contato/admin_filiais/$1';

$route['login'] = 'auth/login';
$route['logout'] = 'auth/logout';

$route['painel/links'] = 'admin_links';
$route['painel/links/(:any)'] = 'admin_links/$1';

$route['newsletters/add'] = 'newsletters/admin_newsletters/add';

$route['painel'] = 'admin_noticias';

$route['painel/newsletter'] = 'newsletters/admin_newsletters';
$route['painel/newsletter/seleciona'] = 'newsletters/admin_newsletters/seleciona';
$route['painel/newsletter/(:any)'] = 'newsletters/admin_newsletters/index/$1';

$route['404_override'] = 'static_pages';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
