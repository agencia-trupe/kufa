<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Detalhes SEO
|
| Configurações básicas de SEO.
|--------------------------------------------------------------------------
*/
$config['site_name'] = 'Kufa, sociedade de advogados';

$config['site_decription'] = '';

$config['site_keywords'] = '';

$config['site_author'] = 'Trupe Agência Criativa - http://trupe.net';
/* End of file seo.php */
/* Location: ./application/config/seo.php */