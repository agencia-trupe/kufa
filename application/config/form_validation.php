<?php 
	$config = array(
    	'parceiros' => array(
        	array(
            	'field' => 'nome',
            	'label' => 'nome',
            	'rules' => 'required',
        	),
        	array(
            	'field' => 'link',
            	'label' => 'link',
            	'rules' => 'required',
        	),
    	),
        'equipe' => array(
            array(
                'field' => 'nome',
                'label' => 'nome',
                'rules' => 'required',
            ),
            array(
                'field' => 'texto',
                'label' => 'descrição',
                'rules' => 'required',
            ),
        ),
        'noticias' => array(
                array(
                    'field' => 'titulo',
                    'label' => 'titulo',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'resumo',
                    'label' => 'resumo',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'conteudo',
                    'label' => 'conteúdo',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'data_publicacao',
                    'label' => 'data de publicação',
                    'rules' => 'required',
                ),
        ),
        'imprensa' => array(
                array(
                    'field' => 'nome',
                    'label' => 'nome',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'ddd1',
                    'label' => 'DDD1',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'telefone1',
                    'label' => 'telefone 1',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'ddd2',
                    'label' => 'DDD2',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'telefone2',
                    'label' => 'telefone 2',
                    'rules' => 'required',
                ),
        ),
        'endereco' => array(
            array(
                'field' => 'rua',
                'label' => 'rua',
                'rules' => 'required' 
                ),
            array(
                'field' => 'bairro',
                'label' => 'bairro',
                'rules' => 'required' 
                ),
            array(
                'field' => 'cidade',
                'label' => 'cidade',
                'rules' => 'required' 
                ),
            array(
                'field' => 'uf',
                'label' => 'uf',
                'rules' => 'required|max_length[2]' 
                ),
            array(
                'field' => 'cep',
                'label' => 'cep',
                'rules' => 'required' 
                ),
            array(
                'field' => 'email',
                'label' => 'email',
                'rules' => 'required' 
                ),
            array(
                'field' => 'telefone',
                'label' => 'telefone',
                'rules' => 'required' 
                ),
        ),
        'filiais' => array(
            array(
                'field' => 'locale',
                'label' => 'local',
                'rules' => 'required' 
                ),
            array(
                'field' => 'contato',
                'label' => 'contato',
                'rules' => 'required' 
                ),
            array(
                'field' => 'endereco',
                'label' => 'endereço',
                'rules' => 'required' 
                ),
            array(
                'field' => 'bairro',
                'label' => 'bairro',
                'rules' => 'required' 
                ),
            array(
                'field' => 'cidade',
                'label' => 'cidade',
                'rules' => 'required' 
                ),
            array(
                'field' => 'uf',
                'label' => 'uf',
                'rules' => 'required|max_length[2]' 
                ),
            array(
                'field' => 'cep',
                'label' => 'cep',
                'rules' => 'required' 
                ),
            array(
                'field' => 'telefone',
                'label' => 'telefone',
                'rules' => 'required' 
                ),
        ),
    );