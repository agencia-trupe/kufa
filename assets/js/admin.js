//Reordenar Serviços
$('.ordenar-equipe').click(function(){
  $("table tbody").sortable({'disabled': false, 'delay':'100'});
  $(this).toggle();
  $('.equipe-mensagem, .salvar-ordem-equipe').toggle();
  return false;
});

$('.salvar-ordem-equipe').click(function(){
  $.ajax({
    type: "POST",
    url: location.protocol + "//" + location.hostname + "/painel/equipe/sort_equipe/",
    data: $("table tbody").sortable("serialize"),
    success: function(status){
      $("table tbody").sortable( "option", "disabled", true );
      $('.equipe-mensagem span').text('Áreas ordenadas com sucesso.');
      $('.salvar-ordem-equipe').toggle();
      $('.ordenar-equipe').toggle();
    }
  });
  return false;
});