 //Retira a margem direita do último elemento do widget de notícias - home/todas
$(".noticia-bloco").first().css({'padding-left' : '0'});
$(".noticia-bloco").last().css({'padding-right' : '0'});
$(".links ul li:first-child").css({'border-top' : 'solid 1px #c9c9c9'});
$(".links .pagination ul li:first-child").css({'border-top' : 'none'});
$(".text article:first-child").css({'border-top' : 'solid 1px #c9c9c9'});
$(".text.post article").css({'border' : 'none'});

(function($) {
    String.prototype.capitalize = function() {
        return this.replace(/^(.)/, function (c) { return c.toUpperCase(); })
    };

    $.fn.equalize = function(hw) {
        if (!hw) {
            hw = 'height';
        }
        var max = 0;
        var prop = (typeof document.body.style.maxHeight != 'undefined' ? 'min' + hw.capitalize() : hw);
        var offset = 'offset' + hw.capitalize();
        this.each(function() {
            var calc = parseInt(this[offset]);
            if (calc > max) {
                max = calc;
            }
        });

        this.each(function() {
            $(this).css(prop, max - (parseInt(this[offset]) - $(this)[hw]()));
        });

        return max;
    };

})(jQuery);

//Fancybox - galerias
$(document).ready(function() {
    $('#banner').cycle({
        fx: 'fade',
        speed: 'slow',
        next: '#control-next',
        prev: '#control-prev',
        before: function (curr, next, opts) {
            var $slideAtual = $(next),
                titulo      = $slideAtual.data('titulo'),
                container   = $('#slide-titulo');
            if (container.text().length <= 1) {
              container.text(titulo).attr('href', $slideAtual.attr('href'));
            } else {
              container.fadeOut('fast', function() {
                container.text(titulo).attr('href', $slideAtual.attr('href'));
              }).fadeIn();
            }
        }
    });

  $('.parceiros-accordion').accordion({
    active : false,
    heightStyle: 'content'
  });

  $('.parceiros-slider').rcarousel({
            visible: 4,
            step: 1,
            speed: 700,
            auto: {
              enabled: true
            },
            width: 180,
            height: 80,
            margin:20
  });
  
  $(".evento-bloco:nth-child(3)").addClass('segundo');
  $("a.noticia-img").fancybox({
    'transitionIn'  :   'fade',
    'transitionOut' :   'fade',
    'nextEffect'    :   'fade', 
    'prevEffect'    :    'fade', 
    'speedIn'       :   600, 
    'speedOut'      :   200, 
    'nextSpeed'     :   60,
    'prevSpeed'     :   60,
    'overlayShow'   :   false,
    'padding'       :   0, 
    'titlePosition' : 'inside',
    'fitToView'     : false,
  });
});

  $(document).ready(function(){
    $("a[rel^='prettyPhoto']").prettyPhoto({
      allow_resize: false,
      overlay_gallery: false,
      show_title: false,
      markup: '<div class="pp_pic_holder"> \
            <div class="ppt">&nbsp;</div> \
            <div class="pp_content_container"> \
                <div class="pp_content"> \
                  <div class="pp_loaderIcon"></div> \
                  <div class="pp_fade"> \
                                    <a class="pp_close" href="#">Close</a> \
                    <a href="#" class="pp_expand" title="Expand the image">Expand</a> \
                    <div class="pp_hoverContainer"> \
                      <a class="pp_next" href="#">next</a> \
                      <a class="pp_previous" href="#">previous</a> \
                    </div> \
                    <div id="pp_full_res"></div> \
                    <div class="pp_details"> \
                      <div class="pp_nav"> \
                        <a href="#" class="pp_arrow_previous">Previous</a> \
                        <p class="currentTextHolder">0/0</p> \
                        <a href="#" class="pp_arrow_next">Next</a> \
                      </div> \
                      <div class="pp_description_wrapper"> \
                      <div class="pp_description" style="display: block"></div> \
                      </div> \
                      {pp_social} \
                    </div> \
                  </div> \
                </div> \
            </div> \
          </div> \
          <div class="pp_overlay"></div>',
    });
  });


//Prepara a requisição ajax do tipo POST para enviar os dados do formulário de
//Newsletters - home
$(function() {
  $('#boletins').submit(function() {
    var form_data = {
      nome : $('.boletim_nome').val(),
      email : $('.boletim_email').val(),
    };
  	$.ajax({
    	url: 'newsletters/add',
    	type: 'POST',
    	async : false,
    	data: form_data,
    	success: function(msg) {
      	alert(msg);
        $('.boletim_nome').val('');
        $('.boletim_email').val('');
    	}
  	});
    return false;
  });
});

//Jqery Accordion - empresas
$(function() {
  $("#accordion").accordion({ 
    autoHeight: false,
    collapsible: true, 
  });
});

$( "#accordion" ).bind( "accordionchangestart", function(event, ui) {
});

//Verifica o suporte ao HTML Placeholder - home/contato
jQuery(function() {
  jQuery.support.placeholder = false;
  test = document.createElement('input');
  if('placeholder' in test) jQuery.support.placeholder = true;
});

//Hack para navegadores que não oferecem suporte ao HTML5 Placeholder - home/contato
$(function() {
  if(!$.support.placeholder) { 
    var active = document.activeElement;
    $(':text').focus(function () {
      if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
        $(this).val('').removeClass('hasPlaceholder');
      }
    }).blur(function () {
      if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
        $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
      }
    });
    $(':text').blur();
    $(active).focus();
    $('form').submit(function () {
      $(this).find('.hasPlaceholder').each(function() { $(this).val(''); });
    });
  }
});



//Prepara a requisição ajax do tipo POST para enviar os dados do formulário de
//contato - contato
$(function() {
  $('#submit').click(function() {
    $('#ajax-loader').show(); 
    var form_data = {
      nome : $('.nome').val(),
      email : $('.email').val(),
      telefone : $('.telefone').val(),
      mensagem : $('.mensagem').val(),
       ajax : '1'
    };
    $.ajax({
      url: "contato/ajax_check",
      type: 'POST',
      async : true,
      data: form_data,
      success: function(msg) {
        $('#message').html(msg);
      }
    });
    return false;
  });
});

//Esconde o gif de ajax loader cada vez qe um arequisição ajax é completada - contato
jQuery(function() {
  $("#ajax-loader").ajaxComplete(function() {
    $(this).hide();
  });
});